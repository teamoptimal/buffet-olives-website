<?php

    namespace App\Http\Controllers;

    use Illuminate\Http\Request;
    use Illuminate\Support\Facades\DB;
    use Illuminate\Support\Facades\Redirect;

    class LoginController extends Controller
    {

        // LOGIN TO CMS
        public function login(Request $request)
        {

            // VALIDATE FIELDS
            $validateFields = ([
                'username' => 'required',
                'password' => 'required'
            ]);
            $this->validate($request, $validateFields);

            // GET USERNAME
            $username = $_POST['username'];

            // GET PASSWORD
            $password = $_POST['password'];

            // GET PASS WITH ABOVE USERNAME
            $savedPass = DB::select("SELECT password FROM users where username='$username'");

            // IF NO MATCHES
            if (sizeof($savedPass) == 0) {
                echo "No user found with those credentials.";
            // IF 1 MATCH
            } else if (sizeof($savedPass) == 1) {

                echo "User found";
                
                // CHECK IF HASHED PASSES MATCH
                // PASS MATCH
                if (password_verify($password, $savedPass[0]->password)) {
                    echo "Password valid!";

                    // GET PASS WITH ABOVE USERNAME
                    $userDetails = DB::select("SELECT name, surname FROM users where username='$username'");

                    session(['name' => $userDetails[0]->name]);
                    session(['surname' => $userDetails[0]->surname]);
                    session(['loggedIn' => true]);
                    return redirect("cmshome");
                
                // PASS NOT MATCH
                } else {
                    echo "Password not valid!";
                }

            // IF MULTIPLE MATCHES
            } else if (sizeof($savedPass) > 1) {
                echo "Error. Too many users found. Please contact administrator.";
            } 


        }

        public static function logout()
        {

            session()->flush();

            return redirect("login");
        }

        public function register(Request $request)
        {

            $hash = password_hash("testpass", PASSWORD_BCRYPT, ['cost' => 12]);

            DB::insert('INSERT INTO users (
                username, password) values (?, ?)',
            [
                "Francorider",
                $hash,

            ]);

        }
    }

?>