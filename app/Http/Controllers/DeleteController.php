<?php

    namespace App\Http\Controllers;

    use Illuminate\Http\Request;
    use Illuminate\Support\Facades\DB;
    use Illuminate\Support\Facades\Redirect;
    use Illuminate\Support\Facades\File;

    class DeleteController extends Controller
    {

        /* HOME */

        // DELETE HOME SLIDER IMAGE
        public function deleteHomeSliderImage(Request $request)
        {

            // VALIDATE FIELDS
            $validateFields = ([
                'img' => "required"
            ]);
            $this->validate($request, $validateFields);

            // GET RECORD WITH IMAGE ID
            $imgurl = DB::table('homesliderimages')->where('id', $request->img)->pluck('imgurl');
            $imgurl = urldecode($imgurl[0]);

            // DELETE RECORD
            DB::table('homesliderimages')->where('id', '=', $request->img)->delete();
            
            if (File::exists(public_path() . $imgurl)) {
                File::delete(public_path() . $imgurl);
            }

            return redirect("homesliderview");
          
        }

        // DELETE HOME CARD IMAGE
        public function deleteHomeCardImage(Request $request)
        {

             // VALIDATE FIELDS
             $validateFields = ([
                'idcard' => "required"
            ]);
            $this->validate($request, $validateFields);

            $img = $request->idcard;

            // GET RECORD WITH IMAGE ID
            $imgurl = DB::table('homecardsimages')->where('id', $img)->pluck('cardimgurl');
            $imgurl = urldecode($imgurl[0]);

            // DELETE RECORD
            DB::table('homecardsimages')->where('id', '=', $img)->delete();
            
            // DELETE FILE
            if (File::exists(public_path() . $imgurl)) {
                File::delete(public_path() . $imgurl);
            }

            echo "success";
          
        }

        /* END HOME */

        /* ABOUT US */

        // DELETE ABOUT US SLIDER IMAGE
        public function deleteAboutUsSliderImage(Request $request)
        {

            // VALIDATE FIELDS
            $validateFields = ([
                'img' => "required"
            ]);
            $this->validate($request, $validateFields);

            // GET RECORD WITH IMAGE ID
            $imgurl = DB::table('aboutussliderimages')->where('id', $request->img)->pluck('imgurl');
            $imgurl = urldecode($imgurl[0]);

            // DELETE RECORD
            DB::table('aboutussliderimages')->where('id', '=', $request->img)->delete();
            
            if (File::exists(public_path() . $imgurl)) {
                File::delete(public_path() . $imgurl);
            }

            return redirect("aboutussliderview");
          
        }

        // DELETE ABOUT US CARD
        public function deleteAboutUsCard(Request $request, $card)
        {

            // GET CARD IMAGES
            $aboutUsCardsImages = DB::table('aboutuscardsimages')->where('idcard', $card)->get();

            // DELETE EACH IMAGES
            foreach ($aboutUsCardsImages as $image) {

                if (File::exists(public_path() . $image->cardimgurl)) {
                    File::delete(public_path() . $image->cardimgurl);
                }

            }

            // DELETE IMAGE RECORDS
            DB::table('aboutuscardsimages')->where('idcard', '=', $card)->delete();

            // DELETE CARD RECORD
            DB::table('aboutuscards')->where('id', '=', $card)->delete();
            
            return redirect("aboutuscardview");
          
        }

        // DELETE ABOUT US CARD IMAGE
        public function deleteAboutUsCardImage(Request $request)
        {

             // VALIDATE FIELDS
             $validateFields = ([
                'idcard' => "required"
            ]);
            $this->validate($request, $validateFields);

            $img = $request->idcard;

            // GET RECORD WITH IMAGE ID
            $imgurl = DB::table('aboutuscardsimages')->where('id', $img)->pluck('cardimgurl');
            $imgurl = urldecode($imgurl[0]);

            // DELETE RECORD
            DB::table('aboutuscardsimages')->where('id', '=', $img)->delete();
            
            // DELETE FILE
            if (File::exists(public_path() . $imgurl)) {
                File::delete(public_path() . $imgurl);
            }

            echo "success";
          
        }

        /* END ABOUT US */

        /* ABOUT OLIVES */

        // DELETE ABOUT OLIVES SLIDER IMAGE
        public function deleteAboutOlivesSliderImage(Request $request)
        {

            // VALIDATE FIELDS
            $validateFields = ([
                'img' => "required"
            ]);
            $this->validate($request, $validateFields);

            // GET RECORD WITH IMAGE ID
            $imgurl = DB::table('aboutolivessliderimages')->where('id', $request->img)->pluck('imgurl');
            $imgurl = urldecode($imgurl[0]);

            // DELETE RECORD
            DB::table('aboutolivessliderimages')->where('id', '=', $request->img)->delete();
            
            if (File::exists(public_path() . $imgurl)) {
                File::delete(public_path() . $imgurl);
            }

            return redirect("aboutolivessliderview");
          
        }

        // DELETE ABOUT OLIVES CARD
        public function deleteAboutOlivesCard(Request $request, $card)
        {

            // GET CARD IMAGES
            $aboutOlivesCardsImages = DB::table('aboutolivescardsimages')->where('idcard', $card)->get();

            // DELETE EACH IMAGES
            foreach ($aboutOlivesCardsImages as $image) {

                if (File::exists(public_path() . $image->cardimgurl)) {
                    File::delete(public_path() . $image->cardimgurl);
                }

            }

            // DELETE IMAGE RECORDS
            DB::table('aboutolivescardsimages')->where('idcard', '=', $card)->delete();

            // DELETE CARD RECORD
            DB::table('aboutolivescards')->where('id', '=', $card)->delete();
            
            return redirect("aboutolivescardview");
          
        }

        // DELETE ABOUT OLIVES CARD IMAGE
        public function deleteAboutOlivesCardImage(Request $request)
        {

             // VALIDATE FIELDS
             $validateFields = ([
                'idcard' => "required"
            ]);
            $this->validate($request, $validateFields);

            $img = $request->idcard;

            // GET RECORD WITH IMAGE ID
            $imgurl = DB::table('aboutolivescardsimages')->where('id', $img)->pluck('cardimgurl');
            $imgurl = urldecode($imgurl[0]);

            // DELETE RECORD
            DB::table('aboutolivescardsimages')->where('id', '=', $img)->delete();
            
            // DELETE FILE
            if (File::exists(public_path() . $imgurl)) {
                File::delete(public_path() . $imgurl);
            }

            echo "success";
          
        }

        /* END ABOUT OLIVES */

        /* RECIPES */

        // DELETE RECIPES SLIDER IMAGE
        public function deleteRecipesSliderImage(Request $request)
        {

            // VALIDATE FIELDS
            $validateFields = ([
                'img' => "required"
            ]);
            $this->validate($request, $validateFields);

            // GET RECORD WITH IMAGE ID
            $imgurl = DB::table('recipessliderimages')->where('id', $request->img)->pluck('imgurl');
            $imgurl = urldecode($imgurl[0]);

            // DELETE RECORD
            DB::table('recipessliderimages')->where('id', '=', $request->img)->delete();
            
            if (File::exists(public_path() . $imgurl)) {
                File::delete(public_path() . $imgurl);
            }

            return redirect("recipessliderview");
          
        }

        // DELETE RECIPES CARD
        public function deleteRecipesCard(Request $request, $card)
        {

            // GET CARD
            $cardRecord = DB::table('recipescards')->where('id', '=', $card)->get();

            // DELETE IMAGE
            if (File::exists(public_path() . $cardRecord[0]->imgurl)) {
                File::delete(public_path() . $cardRecord[0]->imgurl);
            }

            // DELETE CARD RECORD
            DB::table('recipescards')->where('id', '=', $card)->delete();
            
            return redirect("recipescardview");
          
        }

        /* END RECIPES */

        /* KITCHEN FUN */

            // DELETE KITCHEN FUN CARD IMAGE
            public function deleteKitchenFunCardImage(Request $request)
            {

                // VALIDATE FIELDS
                $validateFields = ([
                    'idcard' => "required"
                ]);
                $this->validate($request, $validateFields);

                $img = $request->idcard;

                // GET RECORD WITH IMAGE ID
                $imgurl = DB::table('kitchenfuncardsimages')->where('id', $img)->pluck('cardimgurl');
                $imgurl = urldecode($imgurl[0]);

                // DELETE RECORD
                DB::table('kitchenfuncardsimages')->where('id', '=', $img)->delete();
                
                // DELETE FILE
                if (File::exists(public_path() . $imgurl)) {
                    File::delete(public_path() . $imgurl);
                }

                echo "success";
            
            }

            // DELETE KITCHEN FUN CARD
            public function deleteKitchenFunCard(Request $request, $card)
            {

                // GET CARD IMAGES
                $kitchenFunCardsImages = DB::table('kitchenfuncardsimages')->where('idcard', $card)->get();

                // DELETE EACH IMAGES
                foreach ($kitchenFunCardsImages as $image) {

                    if (File::exists(public_path() . $image->cardimgurl)) {
                        File::delete(public_path() . $image->cardimgurl);
                    }

                }

                // DELETE IMAGE RECORDS
                DB::table('kitchenfuncardsimages')->where('idcard', '=', $card)->delete();

                // DELETE CARD RECORD
                DB::table('kitchenfuncards')->where('id', '=', $card)->delete();
                
                return redirect("kitchenfuncardview");
            
            }


        /* END KITCHEN FUN */

        /* PRODUCT RANGE */

        // DELETE PRODUCT RANGE SLIDER IMAGE
        public function deleteProductRangeSliderImage(Request $request)
        {

            // VALIDATE FIELDS
            $validateFields = ([
                'img' => "required"
            ]);
            $this->validate($request, $validateFields);

            // GET RECORD WITH IMAGE ID
            $imgurl = DB::table('productrangesliderimages')->where('id', $request->img)->pluck('imgurl');
            $imgurl = urldecode($imgurl[0]);

            // DELETE RECORD
            DB::table('productrangesliderimages')->where('id', '=', $request->img)->delete();
            
            if (File::exists(public_path() . $imgurl)) {
                File::delete(public_path() . $imgurl);
            }

            return redirect("productrangesliderview");
          
        }

        // DELETE PRODUCT RANGE CARD
        public function deleteProductRangeCard(Request $request, $card)
        {

            // GET CARD
            $cardRecord = DB::table('productrangecards')->where('id', '=', $card)->get();

            // DELETE IMAGE
            if (File::exists(public_path() . $cardRecord[0]->imgurl)) {
                File::delete(public_path() . $cardRecord[0]->imgurl);
            }

            // DELETE CARD RECORD
            DB::table('productrangecards')->where('id', '=', $card)->delete();
            
            return redirect("productrangecardview");
          
        }

        /* END PRODUCT RANGE */

        /* GALLERY */

            // DELETE GALLERY SLIDER IMAGE
            public function deleteGallerySliderImage(Request $request)
            {

                // VALIDATE FIELDS
                $validateFields = ([
                    'img' => "required"
                ]);
                $this->validate($request, $validateFields);

                // GET RECORD WITH IMAGE ID
                $imgurl = DB::table('gallerysliderimages')->where('id', $request->img)->pluck('imgurl');
                $imgurl = urldecode($imgurl[0]);

                // DELETE RECORD
                DB::table('gallerysliderimages')->where('id', '=', $request->img)->delete();
                
                if (File::exists(public_path() . $imgurl)) {
                    File::delete(public_path() . $imgurl);
                }

                return redirect("gallerysliderview");
            
            }

            // DELETE GALLERY IMAGE
            public function deleteGalleryImage(Request $request)
            {

                // VALIDATE FIELDS
                $validateFields = ([
                    'img' => "required"
                ]);
                $this->validate($request, $validateFields);

                // GET RECORD WITH IMAGE ID
                $imgurl = DB::table('galleryimages')->where('id', $request->img)->pluck('imgurl');
                $imgurl = urldecode($imgurl[0]);

                // DELETE RECORD
                DB::table('galleryimages')->where('id', '=', $request->img)->delete();
                
                if (File::exists(public_path() . $imgurl)) {
                    File::delete(public_path() . $imgurl);
                }

                return redirect("galleryimageview");
            
            }

        /* END GALLERY */
        
    }

?>