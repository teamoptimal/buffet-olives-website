<?php

    namespace App\Http\Controllers;

    use Illuminate\Http\Request;
    use Illuminate\Support\Facades\DB;
    use Illuminate\Support\Facades\Redirect;
    use Illuminate\Support\Facades\File;
    use Mail;

    class UploadController extends Controller
    {

        // CONTACT

        public function send(Request $request) {

            $validateFields = ([
                'name' => 'required',
                'email' => 'required',
                'number' => '',
                'usermessage' => 'required'
            ]);
    
            $this->validate($request, $validateFields);
    
            $name = $_POST['name'];
            $email = $_POST['email'];
            $cell = $_POST['number'];
            $usermessage = $_POST['usermessage'];
    
            Mail::send('email/send', ['name' => $name, 'email' => $email, 'cell' => $cell, 'usermessage' => $usermessage], function ($message)
            {
                // $message->from("info@buffetolives.com", "Buffet Olives");

                $message->from("info@optimalonline.co.za", "Buffet Olives");
    
                $message->to("jennifer@buffetolives.net")->subject('A new message from Buffet Olives Website');
            });

            return redirect("contact");
    
        }

        /* HOME */

            // UPLOAD HOME SLIDER IMAGE
            public function uploadHomeSliderImage(Request $request)
            {

                // VALIDATE FIELDS
                $validateFields = ([
                    'description' => 'required',
                    'image' => 'required',
                    'image' => 'image|mimes:jpeg,jpg,png,gif,svg'
                ]);
                $this->validate($request, $validateFields);

                // GET DESCRIPTION
                $description = $request->description;

                // GET IMAGE NAME
                $imageName = time() . $request->image->getClientOriginalName();

                // UPLOAD IMAGE TO FOLDER
                $upload = $request->image->move(public_path('/images/homesliderimages'), $imageName);

                // INSERT DB RECORD
                $id = DB::table('homesliderimages')->insertGetId(
                    ['description' => $description, 'imgurl' => "/images/homesliderimages/" . $imageName]
                );

                return redirect("homesliderview");
            
            }

            // EDIT HOME SLIDER IMAGE
            public function editHomeSliderImage(Request $request)
            {

                // VALIDATE FIELDS
                $validateFields = ([
                    'imgid' => 'required',
                    'description' => 'required',
                    'image' => 'image|mimes:jpeg,jpg,png,gif,svg'
                ]);
                $this->validate($request, $validateFields);

                // GET ID
                $imgid = $request->imgid;

                // GET DESCRIPTION
                $description = $request->description;

                // IF NEW IMAGE UPLOADED
                if ($request->has('image')){

                    // GET URL OF OLD IMAGE
                    $imgurl = DB::table('homesliderimages')->where('id', $imgid)->pluck('imgurl');

                    // DELETE OLD IMAGE
                    if (File::exists(public_path() . $imgurl[0])) {
                        File::delete(public_path() . $imgurl[0]);
                    }

                    // GET NEW IMAGE NAME
                    $imageName = time() . $request->image->getClientOriginalName();

                    // UPDATE RECORD
                    DB::table('homesliderimages')
                    ->where('id', $imgid)
                    ->update([
                        'description' => $description,
                        'imgurl' => "/images/homesliderimages/" . $imageName
                    ]);

                    // UPLOAD NEW IMAGE TO FOLDER
                    $upload = $request->image->move(public_path('/images/homesliderimages'), $imageName);

                    return redirect("homesliderview");

                } else {

                    // UPDATE RECORD
                    DB::table('homesliderimages')
                    ->where('id', $imgid)
                    ->update([
                        'description' => $description
                    ]);

                    return redirect("homesliderview");

                }
                
            
            }

            // EDIT HOME CARD
            public function editHomeCard(Request $request, $card)
            {

                // VALIDATE FIELDS
                $validateFields = ([
                    'header' => 'required',
                    'text' => 'required',
                    'imgCount' => 'required'
                ]);

                

                $this->validate($request, $validateFields);


                $imgCount = $request->imgCount;

                for ($i = 0, $opCounter= 0; $opCounter <= $imgCount; $i++) {

                    // IF SOME IMAGES ARE STILL NOT UPLOADED
                    if ($opCounter < $imgCount) {

                        $curImgName = 'img' . ($i + 1);

                        // IF IMAGE PRESENT IN REQUEST
                        if ($file = $request->file($curImgName)){

                            $opCounter++;

                            // VALIDATE Image
                            $validateImage = ([
                                'img' . ($i + 1) => 'required',
                                'img' . ($i + 1) => 'image|mimes:jpeg,jpg,png,gif,svg|max:2048',
                            ]);

                            $this->validate($request, $validateImage);

                            // GET IMAGE NAME
                            $imageName = time() . $file->getClientOriginalName();

                            // UPLOAD IMAGE TO FOLDER
                            $upload = $file->move(public_path('/images/homecardsimages'), $imageName);

                            // INSERT DB RECORD
                            $id = DB::table('homecardsimages')->insertGetId(
                                ['idcard' => $card, 'cardimgurl' => "/images/homecardsimages/" . $imageName]
                            );

                        } else {
                            echo "nooooo";
                            die();
                        }
                    } else {
                        break;
                    }

                }

                $header = $request->header;

                $text = $request->text;

                // UPDATE RECORD
                DB::table('homecards')
                ->where('id', $card)
                ->update([
                    'header' => $header,
                    'bodytext' => $text
                ]);
            
                return redirect("homecardview");
            
            }

        /* END HOME */

        /* ABOUT US */

            // UPLOAD ABOUT US SLIDER IMAGE
            public function uploadAboutUsSliderImage(Request $request)
            {

                // VALIDATE FIELDS
                $validateFields = ([
                    'description' => 'required',
                    'image' => 'required',
                    'image' => 'image|mimes:jpeg,jpg,png,gif,svg'
                ]);
                $this->validate($request, $validateFields);

                // GET DESCRIPTION
                $description = $request->description;

                // GET IMAGE NAME
                $imageName = time() . $request->image->getClientOriginalName();

                // UPLOAD IMAGE TO FOLDER
                $upload = $request->image->move(public_path('/images/aboutussliderimages'), $imageName);

                // INSERT DB RECORD
                $id = DB::table('aboutussliderimages')->insertGetId(
                    ['description' => $description, 'imgurl' => "/images/aboutussliderimages/" . $imageName]
                );

                return redirect("aboutussliderview");
            
            }

            // EDIT ABOUT US SLIDER IMAGE
            public function editAboutUsSliderImage(Request $request)
            {

                // VALIDATE FIELDS
                $validateFields = ([
                    'imgid' => 'required',
                    'description' => 'required',
                    'image' => 'image|mimes:jpeg,jpg,png,gif,svg'
                ]);
                $this->validate($request, $validateFields);

                // GET ID
                $imgid = $request->imgid;

                // GET DESCRIPTION
                $description = $request->description;

                // IF NEW IMAGE UPLOADED
                if ($request->has('image')){

                    // GET URL OF OLD IMAGE
                    $imgurl = DB::table('aboutussliderimages')->where('id', $imgid)->pluck('imgurl');

                    // DELETE OLD IMAGE
                    if (File::exists(public_path() . $imgurl[0])) {
                        File::delete(public_path() . $imgurl[0]);
                    }

                    // GET NEW IMAGE NAME
                    $imageName = time() . $request->image->getClientOriginalName();

                    // UPDATE RECORD
                    DB::table('aboutussliderimages')
                    ->where('id', $imgid)
                    ->update([
                        'description' => $description,
                        'imgurl' => "/images/aboutussliderimages/" . $imageName
                    ]);

                    // UPLOAD NEW IMAGE TO FOLDER
                    $upload = $request->image->move(public_path('/images/aboutussliderimages'), $imageName);

                    return redirect("aboutussliderview");

                } else {

                    // UPDATE RECORD
                    DB::table('aboutussliderimages')
                    ->where('id', $imgid)
                    ->update([
                        'description' => $description
                    ]);

                    return redirect("aboutussliderview");

                }
                
            
            }

            // ADD ABOUT US CARD
            public function uploadAboutUsCard(Request $request)
            {

                // VALIDATE FIELDS
                $validateFields = ([
                    'header' => 'required',
                    'text' => 'required',
                    'imgCount' => 'required'
                ]);

                $this->validate($request, $validateFields);

                $header = $request->header;

                $text = $request->text;

                // UPDATE RECORD
                $id = DB::table('aboutuscards')->insertGetId(
                    ['header' => $header,
                    'bodytext' => $text]
                );

                $imgCount = $request->imgCount;

                for ($i = 0, $opCounter= 0; $opCounter <= $imgCount; $i++) {

                    // IF SOME IMAGES ARE STILL NOT UPLOADED
                    if ($opCounter < $imgCount) {

                        $curImgName = 'img' . ($i + 1);

                        // IF IMAGE PRESENT IN REQUEST
                        if ($file = $request->file($curImgName)) {

                            echo "yaas";
                            die();

                            $opCounter++;

                            // VALIDATE Image
                            $validateImage = ([
                                'img' . ($i + 1) => 'required',
                                'img' . ($i + 1) => 'image|mimes:jpeg,jpg,png,gif,svg',
                            ]);

                            $this->validate($request, $validateImage);

                            // GET IMAGE NAME
                            $imageName = time() . $file->getClientOriginalName();

                            // UPLOAD IMAGE TO FOLDER
                            $upload = $file->move(public_path('/images/aboutuscardsimages'), $imageName);

                            // INSERT DB RECORD
                            $imgID = DB::table('aboutuscardsimages')->insertGetId(
                                ['idcard' => $id, 'cardimgurl' => "/images/aboutuscardsimages/" . $imageName]
                            );

                        }
                    } else {
                        break;
                    }

                }
            
                return redirect("aboutuscardview");
            
            }

            // EDIT ABOUT US CARD
            public function editAboutUsCard(Request $request, $card)
            {

                // VALIDATE FIELDS
                $validateFields = ([
                    'header' => 'required',
                    'text' => 'required',
                    'imgCount' => 'required'
                ]);

                $this->validate($request, $validateFields);

                $imgCount = $request->imgCount;

                for ($i = 0, $opCounter= 0; $opCounter <= $imgCount; $i++) {

                    // IF SOME IMAGES ARE STILL NOT UPLOADED
                    if ($opCounter < $imgCount) {

                        $curImgName = 'img' . ($i + 1);

                        // IF IMAGE PRESENT IN REQUEST
                        if ($file = $request->file($curImgName)){

                            $opCounter++;

                            // VALIDATE Image
                            $validateImage = ([
                                'img' . ($i + 1) => 'required',
                                'img' . ($i + 1) => 'image|mimes:jpeg,jpg,png,gif,svg',
                            ]);

                            $this->validate($request, $validateImage);

                            // GET IMAGE NAME
                            $imageName = time() . $file->getClientOriginalName();

                            // UPLOAD IMAGE TO FOLDER
                            $upload = $file->move(public_path('/images/aboutuscardsimages'), $imageName);

                            // INSERT DB RECORD
                            $id = DB::table('aboutuscardsimages')->insertGetId(
                                ['idcard' => $card, 'cardimgurl' => "/images/aboutuscardsimages/" . $imageName]
                            );

                        }
                    } else {
                        break;
                    }

                }

                $header = $request->header;

                $text = $request->text;

                // UPDATE RECORD
                DB::table('aboutuscards')
                ->where('id', $card)
                ->update([
                    'header' => $header,
                    'bodytext' => $text
                ]);
            
                return redirect("aboutuscardview");
            
            }
        
        /* END ABOUT US */

        /* ABOUT OLIVES */

            // UPLOAD ABOUT OLIVES SLIDER IMAGE
            public function uploadAboutOlivesSliderImage(Request $request)
            {

                // VALIDATE FIELDS
                $validateFields = ([
                    'description' => 'required',
                    'image' => 'required',
                    'image' => 'image|mimes:jpeg,jpg,png,gif,svg'
                ]);
                $this->validate($request, $validateFields);

                // GET DESCRIPTION
                $description = $request->description;

                // GET IMAGE NAME
                $imageName = time() . $request->image->getClientOriginalName();

                // UPLOAD IMAGE TO FOLDER
                $upload = $request->image->move(public_path('/images/aboutolivessliderimages'), $imageName);

                // INSERT DB RECORD
                $id = DB::table('aboutolivessliderimages')->insertGetId(
                    ['description' => $description, 'imgurl' => "/images/aboutolivessliderimages/" . $imageName]
                );

                return redirect("aboutolivessliderview");
            
            }

            // EDIT ABOUT OLIVES SLIDER IMAGE
            public function editAboutOlivesSliderImage(Request $request)
            {

                // VALIDATE FIELDS
                $validateFields = ([
                    'imgid' => 'required',
                    'description' => 'required',
                    'image' => 'image|mimes:jpeg,jpg,png,gif,svg'
                ]);
                $this->validate($request, $validateFields);

                // GET ID
                $imgid = $request->imgid;

                // GET DESCRIPTION
                $description = $request->description;

                // IF NEW IMAGE UPLOADED
                if ($request->has('image')){

                    // GET URL OF OLD IMAGE
                    $imgurl = DB::table('aboutolivessliderimages')->where('id', $imgid)->pluck('imgurl');

                    // DELETE OLD IMAGE
                    if (File::exists(public_path() . $imgurl[0])) {
                        File::delete(public_path() . $imgurl[0]);
                    }

                    // GET NEW IMAGE NAME
                    $imageName = time() . $request->image->getClientOriginalName();

                    // UPDATE RECORD
                    DB::table('aboutolivessliderimages')
                    ->where('id', $imgid)
                    ->update([
                        'description' => $description,
                        'imgurl' => "/images/aboutolivessliderimages/" . $imageName
                    ]);

                    // UPLOAD NEW IMAGE TO FOLDER
                    $upload = $request->image->move(public_path('/images/aboutolivessliderimages'), $imageName);

                    return redirect("aboutolivessliderview");

                } else {

                    // UPDATE RECORD
                    DB::table('aboutolivessliderimages')
                    ->where('id', $imgid)
                    ->update([
                        'description' => $description
                    ]);

                    return redirect("aboutolivessliderview");

                }
                
            
            }

            // EDIT ABOUT OLIVES HEADER
            public function editAboutOlivesHeader(Request $request) {

                // VALIDATE FIELDS
                $validateFields = ([
                'heading' => 'required',
                'text' => 'required'
                ]);

                $this->validate($request, $validateFields);

                $heading = $request->heading;

                $text = $request->text;
                
                // UPDATE RECORD
                DB::table('aboutolivesheader')
                ->where('id', 1)
                ->update([
                    'heading' => $heading,
                    'text' => $text
                ]);

                return redirect("aboutolivesheaderedit");
                
            }
            
            // ADD ABOUT OLIVES CARD
            public function uploadAboutOlivesCard(Request $request)
            {

                // VALIDATE FIELDS
                $validateFields = ([
                    'header' => 'required',
                    'text' => 'required',
                    'imgCount' => 'required'
                ]);

                $this->validate($request, $validateFields);

                $header = $request->header;

                $text = $request->text;

                // UPDATE RECORD
                $id = DB::table('aboutolivescards')->insertGetId(
                    ['header' => $header,
                    'bodytext' => $text]
                );

                $imgCount = $request->imgCount;

                for ($i = 0, $opCounter= 0; $opCounter <= $imgCount; $i++) {

                    // IF SOME IMAGES ARE STILL NOT UPLOADED
                    if ($opCounter < $imgCount) {

                        // IF IMAGE PRESENT IN REQUEST
                        if ($request->has('img' . ($i + 1))){

                            $opCounter++;

                            // VALIDATE Image
                            $validateImage = ([
                                'img' . ($i + 1) => 'required',
                                'img' . ($i + 1) => 'image|mimes:jpeg,jpg,png,gif,svg',
                            ]);

                            $this->validate($request, $validateImage);

                            // GET IMAGE NAME
                            $imageName = time() . $request->{'img' . ($i + 1)}->getClientOriginalName();

                            // UPLOAD IMAGE TO FOLDER
                            $upload = $request->{'img' . ($i + 1)}->move(public_path('/images/aboutolivescardsimages'), $imageName);

                            // INSERT DB RECORD
                            $imgID = DB::table('aboutolivescardsimages')->insertGetId(
                                ['idcard' => $id, 'cardimgurl' => "/images/aboutolivescardsimages/" . $imageName]
                            );

                        }
                    } else {
                        break;
                    }

                }
            
                return redirect("aboutolivescardview");
            
            }

            // EDIT ABOUT OLIVES CARD
            public function editAboutOlivesCard(Request $request, $card)
            {

                // VALIDATE FIELDS
                $validateFields = ([
                    'header' => 'required',
                    'text' => 'required',
                    'imgCount' => 'required'
                ]);

                $this->validate($request, $validateFields);

                $imgCount = $request->imgCount;

                for ($i = 0, $opCounter= 0; $opCounter <= $imgCount; $i++) {

                    // IF SOME IMAGES ARE STILL NOT UPLOADED
                    if ($opCounter < $imgCount) {

                        $curImgName = 'img' . ($i + 1);

                        // IF IMAGE PRESENT IN REQUEST
                        if ($file = $request->file($curImgName)){

                            $opCounter++;

                            // VALIDATE Image
                            $validateImage = ([
                                'img' . ($i + 1) => 'required',
                                'img' . ($i + 1) => 'image|mimes:jpeg,jpg,png,gif,svg',
                            ]);

                            $this->validate($request, $validateImage);

                            // GET IMAGE NAME
                            $imageName = time() . $file->getClientOriginalName();

                            // UPLOAD IMAGE TO FOLDER
                            $upload = $file->move(public_path('/images/aboutolivescardsimages'), $imageName);

                            // INSERT DB RECORD
                            $id = DB::table('aboutolivescardsimages')->insertGetId(
                                ['idcard' => $card, 'cardimgurl' => "/images/aboutolivescardsimages/" . $imageName]
                            );

                        }
                    } else {
                        break;
                    }

                }

                $header = $request->header;

                $text = $request->text;

                // UPDATE RECORD
                DB::table('aboutolivescards')
                ->where('id', $card)
                ->update([
                    'header' => $header,
                    'bodytext' => $text
                ]);
            
                return redirect("aboutolivescardview");
            
            }

            // EDIT ABOUT OLIVES FOOTER
            public function editAboutOlivesFooter(Request $request) {

                // VALIDATE FIELDS
                $validateFields = ([
                'heading' => 'required',
                'text' => 'required'
                ]);

                $this->validate($request, $validateFields);

                $heading = $request->heading;

                $text = $request->text;
                
                // UPDATE RECORD
                DB::table('aboutolivesfooter')
                ->where('id', 1)
                ->update([
                    'heading' => $heading,
                    'text' => $text
                ]);

                return redirect("aboutolivesfooteredit");
                
            }

        /* END ABOUT OLIVES */

        /* RECIPES */

            // UPLOAD RECIPES SLIDER IMAGE
            public function uploadRecipesSliderImage(Request $request)
            {

                // VALIDATE FIELDS
                $validateFields = ([
                    'description' => 'required',
                    'image' => 'required',
                    'image' => 'image|mimes:jpeg,jpg,png,gif,svg'
                ]);
                $this->validate($request, $validateFields);

                // GET DESCRIPTION
                $description = $request->description;

                // GET IMAGE NAME
                $imageName = time() . $request->image->getClientOriginalName();

                // UPLOAD IMAGE TO FOLDER
                $upload = $request->image->move(public_path('/images/recipessliderimages'), $imageName);

                // INSERT DB RECORD
                $id = DB::table('recipessliderimages')->insertGetId(
                    ['description' => $description, 'imgurl' => "/images/recipessliderimages/" . $imageName]
                );

                return redirect("recipessliderview");
            
            }

            // EDIT RECIPES SLIDER IMAGE
            public function editRecipesSliderImage(Request $request)
            {

                // VALIDATE FIELDS
                $validateFields = ([
                    'imgid' => 'required',
                    'description' => 'required',
                    'image' => 'image|mimes:jpeg,jpg,png,gif,svg'
                ]);
                $this->validate($request, $validateFields);

                // GET ID
                $imgid = $request->imgid;

                // GET DESCRIPTION
                $description = $request->description;

                // IF NEW IMAGE UPLOADED
                if ($request->has('image')){

                    // GET URL OF OLD IMAGE
                    $imgurl = DB::table('recipessliderimages')->where('id', $imgid)->pluck('imgurl');

                    // DELETE OLD IMAGE
                    if (File::exists(public_path() . $imgurl[0])) {
                        File::delete(public_path() . $imgurl[0]);
                    }

                    // GET NEW IMAGE NAME
                    $imageName = time() . $request->image->getClientOriginalName();

                    // UPDATE RECORD
                    DB::table('recipessliderimages')
                    ->where('id', $imgid)
                    ->update([
                        'description' => $description,
                        'imgurl' => "/images/recipessliderimages/" . $imageName
                    ]);

                    // UPLOAD NEW IMAGE TO FOLDER
                    $upload = $request->image->move(public_path('/images/recipessliderimages'), $imageName);

                    return redirect("recipessliderview");

                } else {

                    // UPDATE RECORD
                    DB::table('recipessliderimages')
                    ->where('id', $imgid)
                    ->update([
                        'description' => $description
                    ]);

                    return redirect("recipessliderview");

                }
                
            }

            // ADD Recipes CARD
            public function uploadRecipesCard(Request $request)
            {

                // VALIDATE FIELDS
                $validateFields = ([
                    'header' => 'required',
                    'description' => 'required',
                    'img' => 'required',
                    'img' => 'image|mimes:jpeg,jpg,png,gif,svg',
                    'ingredients' => "required",
                    'method' => 'required'

                ]);

                $this->validate($request, $validateFields);

                $header = $request->header;

                $description = $request->description;

                $ingredients = $request->ingredients;

                $method = $request->method;

                // GET IMAGE NAME
                $imageName = time() . $request->{'img'}->getClientOriginalName();

                // UPLOAD IMAGE TO FOLDER
                $upload = $request->{'img'}->move(public_path('/images/recipescardsimages'), $imageName);

                // INSERT DB RECORD
                $id = DB::table('recipescards')->insertGetId(
                    ['header' => $header,
                    'description' => $description,
                    'imgurl' => "/images/recipescardsimages/" . $imageName,
                    'ingredients' => $ingredients,
                    'method' => $method
                    ]
                );

                return redirect("recipescardview");
            
            }

            // EDIT RECIPES CARD
            public function editRecipesCard(Request $request, $card)
            {

                // VALIDATE FIELDS
                $validateFields = ([
                    'header' => 'required',
                    'description' => 'required',
                    'img' => 'image|mimes:jpeg,jpg,png,gif,svg',
                    'ingredients' => "required",
                    'method' => 'required'
                ]);

                $this->validate($request, $validateFields);

                $header = $request->header;

                $description = $request->description;

                $ingredients = $request->ingredients;

                $method = $request->method;

                $shouldAddImg = false;


                // IF IMAGE PRESENT IN REQUEST
                if ($file = $request->file('img')){

                    $shouldAddImg = true;

                    // GET IMAGE NAME
                    $imageName = time() . $file->getClientOriginalName();

                    // UPLOAD IMAGE TO FOLDER
                    $upload = $file->move(public_path('/images/recipescardsimages'), $imageName);

                    // GET URL OF OLD IMAGE
                    $imgurl = DB::table('recipescards')->where('id', $card)->pluck('imgurl');

                    // DELETE OLD IMAGE
                    if (File::exists(public_path() . $imgurl[0])) {
                        File::delete(public_path() . $imgurl[0]);
                    }
                    
                }

                if ($shouldAddImg) {

                    // UPDATE RECORD
                    DB::table('recipescards')
                    ->where('id', $card)
                    ->update([
                        'header' => $header,
                        'description' => $description,
                        'imgurl' => "/images/recipescardsimages/" . $imageName,
                        'ingredients' => $ingredients,
                        'method' => $method
                    ]); 

                } else {

                    // UPDATE RECORD
                    DB::table('recipescards')
                    ->where('id', $card)
                    ->update([
                        'header' => $header,
                        'description' => $description,
                        'ingredients' => $ingredients,
                        'method' => $method
                    ]); 

                }

                return redirect("recipescardview");
            
            }

        /* END RECIPES */

        /* KITCHEN FUN */

            public function editKitchenFunHeader(Request $request) {

                // VALIDATE FIELDS
                $validateFields = ([
                    'description' => 'required',
                    'img' => 'image|mimes:jpeg,jpg,png,gif,svg',
                ]);
                

                $this->validate($request, $validateFields);

                $description = $request->description;

                if ($request->has('img')) {

                    echo "sup";
                    die();

                    // GET IMAGE NAME
                    $imageName = time() . $request->{'img'}->getClientOriginalName();

                    // UPLOAD IMAGE TO FOLDER
                    $upload = $request->{'img'}->move(public_path('/images/kitchenfunheaderimage'), $imageName);

                    // GET URL OF OLD IMAGE
                    $imgurl = DB::table('kitchenfunheaderimage')->where('id', 1)->pluck('imgurl');

                    // DELETE OLD IMAGE
                    if (File::exists(public_path() . $imgurl[0])) {
                        File::delete(public_path() . $imgurl[0]);
                    }

                    // UPDATE RECORD
                    DB::table('kitchenfunheaderimage')
                    ->where('id', 1)
                    ->update([
                        'description' => $description,
                        'imgurl' => '/images/kitchenfunheaderimage/' . $imageName
                    ]); 


                } else {

                    echo "nnooo";
                    die();

                    // UPDATE RECORD
                    DB::table('kitchenfunheaderimage')
                    ->where('id', 1)
                    ->update([
                        'description' => $description
                    ]); 

                }

                return redirect("kitchenfunheaderedit");

            }

            // ADD KITCHEN FUN CARD
            public function uploadKitchenFunCard(Request $request)
            {

                // VALIDATE FIELDS
                $validateFields = ([
                    'header' => 'required',
                    'text' => 'required',
                    'pdf' => 'required',
                    'pdf' => 'mimes:pdf',
                    'imgCount' => 'required'
                ]);

                $this->validate($request, $validateFields);

                $header = $request->header;

                $text = $request->text;

                // GET PDF NAME
                $pdfName = time() . $request->{'pdf'}->getClientOriginalName();

                // UPLOAD PDF TO FOLDER
                $upload = $request->{'pdf'}->move(public_path('/images/kitchenfunpdfs'), $pdfName);

                // ADD RECORD
                $id = DB::table('kitchenfuncards')->insertGetId(
                    ['header' => $header,
                    'bodytext' => $text,
                    'pdfurl' => '/images/kitchenfunpdfs' . $pdfName]
                );

                $imgCount = $request->imgCount;

                for ($i = 0, $opCounter= 0; $opCounter <= $imgCount; $i++) {

                    // IF SOME IMAGES ARE STILL NOT UPLOADED
                    if ($opCounter < $imgCount) {

                        // IF IMAGE PRESENT IN REQUEST
                        if ($request->has('img' . ($i + 1))){

                            $opCounter++;

                            // VALIDATE Image
                            $validateImage = ([
                                'img' . ($i + 1) => 'required',
                                'img' . ($i + 1) => 'image|mimes:jpeg,jpg,png,gif,svg',
                            ]);

                            $this->validate($request, $validateImage);

                            // GET IMAGE NAME
                            $imageName = time() . $request->{'img' . ($i + 1)}->getClientOriginalName();

                            // UPLOAD IMAGE TO FOLDER
                            $upload = $request->{'img' . ($i + 1)}->move(public_path('/images/kitchenfuncardsimages'), $imageName);

                            // INSERT DB RECORD
                            $imgID = DB::table('kitchenfuncardsimages')->insertGetId(
                                ['idcard' => $id, 'cardimgurl' => "/images/kitchenfuncardsimages/" . $imageName]
                            );

                        }
                    } else {
                        break;
                    }

                }
            
                return redirect("kitchenfuncardview");
            
            }

            // EDIT KITCHEN FUN CARD
            public function editKitchenFunCard(Request $request, $card)
            {

                // VALIDATE FIELDS
                $validateFields = ([
                    'header' => 'required',
                    'text' => 'required',
                    'imgCount' => 'required',
                    'pdf' => 'mimes:pdf'
                ]);

                $this->validate($request, $validateFields);

                $imgCount = $request->imgCount;

                for ($i = 0, $opCounter= 0; $opCounter <= $imgCount; $i++) {

                    // IF SOME IMAGES ARE STILL NOT UPLOADED
                    if ($opCounter < $imgCount) {

                        $curImgName = 'img' . ($i + 1);

                        // IF IMAGE PRESENT IN REQUEST
                        if ($file = $request->file($curImgName)){


                            $opCounter++;

                            // VALIDATE Image
                            $validateImage = ([
                                'img' . ($i + 1) => 'required',
                                'img' . ($i + 1) => 'image|mimes:jpeg,jpg,png,gif,svg',
                            ]);

                            $this->validate($request, $validateImage);

                            // GET IMAGE NAME
                            $imageName = time() . $file->getClientOriginalName();

                            // UPLOAD IMAGE TO FOLDER
                            $upload = $file->move(public_path('/images/kitchenfuncardsimages'), $imageName);

                            // INSERT DB RECORD
                            $id = DB::table('kitchenfuncardsimages')->insertGetId(
                                ['idcard' => $card, 'cardimgurl' => "/images/kitchenfuncardsimages/" . $imageName]
                            );

                        }
                    } else {
                        break;
                    }

                }

                $header = $request->header;

                $text = $request->text;

                if ($request->has('pdf')) {

                    // GET IMAGE NAME
                    $pdfName = time() . $request->{'pdf'}->getClientOriginalName();

                    // UPLOAD IMAGE TO FOLDER
                    $upload = $request->{'pdf'}->move(public_path('/images/kitchenfunpdfs'), $pdfName);

                    // GET OLD PDF
                    $oldpdfurl = DB::table('kitchenfuncards')->where('id', $card)->pluck('pdfurl');
                    $oldpdfurl = urldecode($oldpdfurl[0]);
                    
                    // DELETE FILE
                    if (File::exists(public_path() . $oldpdfurl)) {
                        File::delete(public_path() . $oldpdfurl);
                    }

                    // UPDATE RECORD
                    DB::table('kitchenfuncards')
                    ->where('id', $card)
                    ->update([
                        'header' => $header,
                        'bodytext' => $text,
                        'pdfurl' => '/images/kitchenfunpdfs/' . $pdfName
                    ]);

                } else {

                    // UPDATE RECORD
                    DB::table('kitchenfuncards')
                    ->where('id', $card)
                    ->update([
                        'header' => $header,
                        'bodytext' => $text
                    ]);
                    
                }
        
                return redirect("kitchenfuncardview");
            
            }

        /* END KITCHEN FUN */

        /* PRODUCT RANGE */

            // UPLOAD PRODUCT RANGE SLIDER IMAGE
            public function uploadProductRangeSliderImage(Request $request)
            {

                // VALIDATE FIELDS
                $validateFields = ([
                    'description' => 'required',
                    'image' => 'required',
                    'image' => 'image|mimes:jpeg,jpg,png,gif,svg'
                ]);
                $this->validate($request, $validateFields);

                // GET DESCRIPTION
                $description = $request->description;

                // GET IMAGE NAME
                $imageName = time() . $request->image->getClientOriginalName();

                // UPLOAD IMAGE TO FOLDER
                $upload = $request->image->move(public_path('/images/productrangesliderimages'), $imageName);

                // INSERT DB RECORD
                $id = DB::table('productrangesliderimages')->insertGetId(
                    ['description' => $description, 'imgurl' => "/images/productrangesliderimages/" . $imageName]
                );

                return redirect("productrangesliderview");
            
            }

            // ADD PRODUCT RANGE CARD
            public function uploadProductRangeCard(Request $request)
            {

                // VALIDATE FIELDS
                $validateFields = ([
                    'header' => 'required',
                    'text' => 'required',
                    'img' => 'required|max:1024',
                    'image' => 'image|mimes:jpeg,jpg,png,gif,svg'
                ]);

                $this->validate($request, $validateFields);

                $header = $request->header;

                $text = $request->text;

                // GET IMAGE NAME
                $imageName = time() . $request->{'img'}->getClientOriginalName();

                // UPLOAD IMAGE TO FOLDER
                $upload = $request->{'img'}->move(public_path('/images/productrangecardsimages'), $imageName);

                // INSERT DB RECORD
                $id = DB::table('productrangecards')->insertGetId(
                    ['header' => $header,
                    'bodytext' => $text,
                    'imgurl' => "/images/productrangecardsimages/" . $imageName
                    ]);

                return redirect("productrangecardview");
            
            }

            // EDIT PRODUCT RANGE CARD
            public function editProductRangeCard(Request $request, $card)
            {

                // VALIDATE FIELDS
                $validateFields = ([
                    'header' => 'required',
                    'text' => 'required',
                    'img' => 'image|mimes:jpeg,jpg,png,gif,svg'
                ]);

                $this->validate($request, $validateFields);

                $header = $request->header;

                $text = $request->text;

                $shouldAddImg = false;

                // IF IMAGE PRESENT IN REQUEST
                if ($file = $request->file('img')){


                    $shouldAddImg = true;

                    // GET IMAGE NAME
                    $imageName = time() . $file->getClientOriginalName();

                    // UPLOAD IMAGE TO FOLDER
                    $upload = $file->move(public_path('/images/productrangecardsimages'), $imageName);

                    // GET URL OF OLD IMAGE
                    $imgurl = DB::table('productrangecards')->where('id', $card)->pluck('imgurl');

                    // DELETE OLD IMAGE
                    if (File::exists(public_path() . $imgurl[0])) {
                        File::delete(public_path() . $imgurl[0]);
                    }
                    
                }


                if ($shouldAddImg) {

                // UPDATE RECORD
                DB::table('productrangecards')
                ->where('id', $card)
                ->update([
                    'header' => $header,
                    'bodytext' => $text,
                    'imgurl' => "/images/productrangecardsimages/" . $imageName
                ]); 

                } else {

                    // UPDATE RECORD
                DB::table('productrangecards')
                ->where('id', $card)
                ->update([
                    'header' => $header,
                    'bodytext' => $text
                ]); 

                }

                return redirect("productrangecardview");
            
            }

        /* END PRODUCT RANGE */

        /* GALLERY */

            // UPLOAD GALLERY SLIDER IMAGE
            public function uploadGallerySliderImage(Request $request)
            {

                // VALIDATE FIELDS
                $validateFields = ([
                    'description' => 'required',
                    'image' => 'required',
                    'image' => 'image|mimes:jpeg,jpg,png,gif,svg'
                ]);
                $this->validate($request, $validateFields);

                // GET DESCRIPTION
                $description = $request->description;

                // GET IMAGE NAME
                $imageName = time() . $request->image->getClientOriginalName();

                // UPLOAD IMAGE TO FOLDER
                $upload = $request->image->move(public_path('/images/gallerysliderimages'), $imageName);

                // INSERT DB RECORD
                $id = DB::table('gallerysliderimages')->insertGetId(
                    ['description' => $description, 'imgurl' => "/images/gallerysliderimages/" . $imageName]
                );

                return redirect("gallerysliderview");
            }

            // UPLOAD GALLERY IMAGE
            public function uploadGalleryImage(Request $request)
            {

                // VALIDATE FIELDS
                $validateFields = ([
                    'description' => 'required',
                    'image' => 'required',
                    'image' => 'image|mimes:jpeg,jpg,png,gif,svg'
                ]);
                $this->validate($request, $validateFields);

                // GET DESCRIPTION
                $description = $request->description;

                // GET IMAGE NAME
                $imageName = time() . $request->image->getClientOriginalName();

                // UPLOAD IMAGE TO FOLDER
                $upload = $request->image->move(public_path('/images/galleryimages'), $imageName);

                // INSERT DB RECORD
                $id = DB::table('galleryimages')->insertGetId(
                    ['description' => $description, 'imgurl' => "/images/galleryimages/" . $imageName]
                );

                return redirect("galleryimageview");
            }

        /* END GALLERY */

        /* SOCIAL */

            // EDIT SOCIAL HEADER
            public function editSocialHeaderImage(Request $request) {

                // VALIDATE FIELDS
                $validateFields = ([
                    'description' => 'required',
                    'img' => 'image|mimes:jpeg,jpg,png,gif,svg',
                ]);

                $this->validate($request, $validateFields);

                $description = $request->description;

                if ($request->has('img')) {

                    // GET IMAGE NAME
                    $imageName = time() . $request->{'img'}->getClientOriginalName();

                    // UPLOAD IMAGE TO FOLDER
                    $upload = $request->{'img'}->move(public_path('/images/socialheaderimage'), $imageName);

                    // GET URL OF OLD IMAGE
                    $imgurl = DB::table('socialheaderimage')->where('id', 1)->pluck('imgurl');

                    // DELETE OLD IMAGE
                    if (File::exists(public_path() . $imgurl[0])) {
                        File::delete(public_path() . $imgurl[0]);
                    }

                    // UPDATE RECORD
                    DB::table('socialheaderimage')
                    ->where('id', 1)
                    ->update([
                        'description' => $description,
                        'imgurl' => '/images/socialheaderimage/' . $imageName
                    ]); 


                } else {

                    // UPDATE RECORD
                    DB::table('socialheaderimage')
                    ->where('id', 1)
                    ->update([
                        'description' => $description
                    ]); 

                }

                return redirect("socialheaderedit");


            }

        /* END SOCIAL */

        /* CONTACT */

            // EDIT CONTACT HEADER
            public function editContactHeaderImage(Request $request) {

                // VALIDATE FIELDS
                $validateFields = ([
                    'description' => 'required',
                    'img' => 'image|mimes:jpeg,jpg,png,gif,svg|max: 1024',
                ]);

                $this->validate($request, $validateFields);

                $description = $request->description;

                if ($request->has('img')) {

                    // GET IMAGE NAME
                    $imageName = time() . $request->{'img'}->getClientOriginalName();

                    // UPLOAD IMAGE TO FOLDER
                    $upload = $request->{'img'}->move(public_path('/images/contactheaderimage'), $imageName);

                    // GET URL OF OLD IMAGE
                    $imgurl = DB::table('contactheaderimage')->where('id', 1)->pluck('imgurl');

                    // DELETE OLD IMAGE
                    if (File::exists(public_path() . $imgurl[0])) {
                        File::delete(public_path() . $imgurl[0]);
                    }

                    // UPDATE RECORD
                    DB::table('contactheaderimage')
                    ->where('id', 1)
                    ->update([
                        'description' => $description,
                        'imgurl' => '/images/contactheaderimage/' . $imageName
                    ]); 


                } else {

                    // UPDATE RECORD
                    DB::table('contactheaderimage')
                    ->where('id', 1)
                    ->update([
                        'description' => $description
                    ]); 

                }

                return redirect("contactheaderedit");


            }

        /* END CONTACT */
        
    }

?>