<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Input;

// VIEWS

// HOME PAGE
Route::get('/', function () {
    return view('home');
});

// ABOUT US PAGE
Route::get('/aboutus', function () {
    return view('aboutus');
});

// ABOUT OLIVES PAGE
Route::get('/aboutolives', function () {
    return view('aboutolives');
});

// RECIPES PAGE
Route::get('/recipes', function () {
    return view('recipes');
});

// VIEW RECIPE PAGE
Route::get('/recipe/{cardid}', function ($cardid) {
    return view('/recipe', ["cardid" => $cardid]);

});

// VIEW USER RECIPE PAGE
Route::get('/userrecipe/{cardid}', function ($cardid) {
    return view('/userrecipe', ["cardid" => $cardid]);

});

// KITCHEN PAGE
Route::get('/kitchenfun', function () {
    return view('kitchenfun');
});


// PRODUCT RANGE PAGE
Route::get('/productrange', function () {
    return view('productrange');
});

// GALLERY PAGE
Route::get('/gallery', function () {
    return view('gallery');
});

// SOCIAL FEED PAGE
Route::get('/social', function () {
    return view('social');
});

// CONTACT PAGE
Route::get('/contact', function () {
    return view('contact');
});

// ASPIRING CHEFS PAGE
Route::get('/aspiringchefs', function () {
    return view('aspiringchefs');
});

// CONTACT
Route::post('/send', 'UploadController@send');

// CMS
// LOGIN TO CMS
Route::get('/login', function () {
    return view('/cms/login');
});

// CMS HOME
Route::get('/cmshome', function () {

    // IF LOGGED IN
    if (session()->has('loggedIn')) {
        if (session('loggedIn')) {
            return view('/cms/cmshome');
        } else {
            return Redirect('/dologout'); 
        }
    } else {
        return Redirect('/dologout'); 
    } 
});

/* HOME */

    // VIEW HOME SLIDER IMAGES
    Route::get('/homesliderview', function () {
        if (session()->has('loggedIn')) {
            if (session('loggedIn')) {
                return view('/cms/homesliderview');
            } else {
                return Redirect('/dologout'); 
            }
        } else {
            return Redirect('/dologout'); 
        } 
    });

    // ADD HOME SLIDER IMAGE
    Route::get('/homeslideradd', function () {
        if (session()->has('loggedIn')) {
            if (session('loggedIn')) {
                return view('/cms/homeslideradd');
            } else {
                return Redirect('/dologout'); 
            }
        } else {
            return Redirect('/dologout'); 
        } 
    });

    // EDIT HOME CARD
    Route::get('/homecardedit/{card}', function ($card) {
        if (session()->has('loggedIn')) {
            if (session('loggedIn')) {
                return view('/cms/homecardedit', ["card" => $card]);
            } else {
                return Redirect('/dologout'); 
            }
        } else {
            return Redirect('/dologout'); 
        } 
    });

    // VIEW HOME CARDS
    Route::get('/homecardview', function () {
        if (session()->has('loggedIn')) {
            if (session('loggedIn')) {
                return view('/cms/homecardview');
            } else {
                return Redirect('/dologout'); 
            }
        } else {
            return Redirect('/dologout'); 
        } 
    });

/* END HOME */

/* ABOUT US */

    // VIEW ABOUT US SLIDER IMAGES
    Route::get('/aboutussliderview', function () {
        if (session()->has('loggedIn')) {
            if (session('loggedIn')) {
                return view('/cms/aboutussliderview');
            } else {
                return Redirect('/dologout'); 
            }
        } else {
            return Redirect('/dologout'); 
        } 
    });

    // VIEW ABOUT US CARDS
    Route::get('/aboutuscardview', function () {
        if (session()->has('loggedIn')) {
            if (session('loggedIn')) {
                return view('/cms/aboutuscardview');
            } else {
                return Redirect('/dologout'); 
            }
        } else {
            return Redirect('/dologout'); 
        } 
    });

    // ADD ABOUT US SLIDER IMAGE
    Route::get('/aboutusslideradd', function () {
        if (session()->has('loggedIn')) {
            if (session('loggedIn')) {
                return view('/cms/aboutusslideradd');
            } else {
                return Redirect('/dologout'); 
            }
        } else {
            return Redirect('/dologout'); 
        } 
    });

    // ADD ABOUT US CARD
    Route::get('/aboutuscardadd', function () {
        if (session()->has('loggedIn')) {
            if (session('loggedIn')) {
                return view('/cms/aboutuscardadd');
            } else {
                return Redirect('/dologout'); 
            }
        } else {
            return Redirect('/dologout'); 
        } 
    });

    // EDIT ABOUT US CARD
    Route::get('/aboutuscardedit/{card}', function ($card) {
        if (session()->has('loggedIn')) {
            if (session('loggedIn')) {
                return view('/cms/aboutuscardedit', ["card" => $card]);
            } else {
                return Redirect('/dologout'); 
            }
        } else {
            return Redirect('/dologout'); 
        } 
    });

/* END ABOUT US */

/* ABOUT OLIVES */

    // VIEW ABOUT OLIVES SLIDER IMAGES
    Route::get('/aboutolivessliderview', function () {
        if (session()->has('loggedIn')) {
            if (session('loggedIn')) {
                return view('/cms/aboutolivessliderview');
            } else {
                return Redirect('/dologout'); 
            }
        } else {
            return Redirect('/dologout'); 
        } 
    });

    // VIEW ABOUT OLIVES CARDS
    Route::get('/aboutolivescardview', function () {
        if (session()->has('loggedIn')) {
            if (session('loggedIn')) {
                return view('/cms/aboutolivescardview');
            } else {
                return Redirect('/dologout'); 
            }
        } else {
            return Redirect('/dologout'); 
        } 
    });

    // ADD ABOUT OLIVES SLIDER IMAGE
    Route::get('/aboutolivesslideradd', function () {
        if (session()->has('loggedIn')) {
            if (session('loggedIn')) {
                return view('/cms/aboutolivesslideradd');
            } else {
                return Redirect('/dologout'); 
            }
        } else {
            return Redirect('/dologout'); 
        } 
    });

    // EDIT ABOUT OLIVES HEADER
    Route::get('/aboutolivesheaderedit', function () {
        if (session()->has('loggedIn')) {
            if (session('loggedIn')) {
                return view('/cms/aboutolivesheaderedit');
            } else {
                return Redirect('/dologout'); 
            }
        } else {
            return Redirect('/dologout'); 
        } 
    });

    // ADD ABOUT OLIVES CARD
    Route::get('/aboutolivescardadd', function () {
        if (session()->has('loggedIn')) {
            if (session('loggedIn')) {
                return view('/cms/aboutolivescardadd');
            } else {
                return Redirect('/dologout'); 
            }
        } else {
            return Redirect('/dologout'); 
        } 
    });

    // EDIT ABOUT OLIVES CARD
    Route::get('/aboutolivescardedit/{card}', function ($card) {
        if (session()->has('loggedIn')) {
            if (session('loggedIn')) {
                return view('/cms/aboutolivescardedit', ["card" => $card]);
            } else {
                return Redirect('/dologout'); 
            }
        } else {
            return Redirect('/dologout'); 
        } 
    });

    // EDIT ABOUT OLIVES FOOTER
    Route::get('/aboutolivesfooteredit', function () {
        if (session()->has('loggedIn')) {
            if (session('loggedIn')) {
                return view('/cms/aboutolivesfooteredit');
            } else {
                return Redirect('/dologout'); 
            }
        } else {
            return Redirect('/dologout'); 
        } 
    });

/* END ABOUT OLIVES */

/* RECIPES */

    // VIEW RECIPES SLIDER IMAGES
    Route::get('/recipessliderview', function () {
        if (session()->has('loggedIn')) {
            if (session('loggedIn')) {
                return view('/cms/recipessliderview');
            } else {
                return Redirect('/dologout'); 
            }
        } else {
            return Redirect('/dologout'); 
        } 
    });

    // ADD RECIPES SLIDER IMAGE
    Route::get('/recipesslideradd', function () {
        if (session()->has('loggedIn')) {
            if (session('loggedIn')) {
                return view('/cms/recipesslideradd');
            } else {
                return Redirect('/dologout'); 
            }
        } else {
            return Redirect('/dologout'); 
        } 
    });

    // VIEW RECIPES CARDS
    Route::get('/recipescardview', function () {
        if (session()->has('loggedIn')) {
            if (session('loggedIn')) {
                return view('/cms/recipescardview');
            } else {
                return Redirect('/dologout'); 
            }
        } else {
            return Redirect('/dologout'); 
        } 
    });


    // ADD RECIPES CARD
    Route::get('/recipescardadd', function () {
        if (session()->has('loggedIn')) {
            if (session('loggedIn')) {
                return view('/cms/recipescardadd');
            } else {
                return Redirect('/dologout'); 
            }
        } else {
            return Redirect('/dologout'); 
        } 
    });

    // EDIT RECIPES CARD
    Route::get('/recipescardedit/{card}', function ($card) {
        if (session()->has('loggedIn')) {
            if (session('loggedIn')) {
                return view('/cms/recipescardedit', ["card" => $card]);
            } else {
                return Redirect('/dologout'); 
            }
        } else {
            return Redirect('/dologout'); 
        } 
    });


/* END RECIPES */

/* KITCHEN FUN */

    // EDIT KITCHEN FUN SLIDER IMAGES
    Route::get('/kitchenfunheaderedit', function () {
        if (session()->has('loggedIn')) {
            if (session('loggedIn')) {
                return view('/cms/kitchenfunheaderedit');
            } else {
                return Redirect('/dologout'); 
            }
        } else {
            return Redirect('/dologout'); 
        } 
    });

    // VIEW KITCHEN FUN CARDS
    Route::get('/kitchenfuncardview', function () {
        if (session()->has('loggedIn')) {
            if (session('loggedIn')) {
                return view('/cms/kitchenfuncardview');
            } else {
                return Redirect('/dologout'); 
            }
        } else {
            return Redirect('/dologout'); 
        } 
    });

    // ADD KITCHEN FUN CARD
    Route::get('/kitchenfuncardadd', function () {
        if (session()->has('loggedIn')) {
            if (session('loggedIn')) {
                return view('/cms/kitchenfuncardadd');
            } else {
                return Redirect('/dologout'); 
            }
        } else {
            return Redirect('/dologout'); 
        } 
    });

    // EDIT KITCHEN FUN CARD
    Route::get('/kitchenfuncardedit/{card}', function ($card) {
        if (session()->has('loggedIn')) {
            if (session('loggedIn')) {
                return view('/cms/kitchenfuncardedit', ["card" => $card]);
            } else {
                return Redirect('/dologout'); 
            }
        } else {
            return Redirect('/dologout'); 
        } 
    });

/* END KITCHEN FUN */

/* PRODUCT RANGE */

    // VIEW PRODUCT RANGE SLIDER IMAGES
    Route::get('/productrangesliderview', function () {
        if (session()->has('loggedIn')) {
            if (session('loggedIn')) {
                return view('/cms/productrangesliderview');
            } else {
                return Redirect('/dologout'); 
            }
        } else {
            return Redirect('/dologout'); 
        } 
    });

    // ADD PRODUCT RANGE SLIDER IMAGE
    Route::get('/productrangeslideradd', function () {
        if (session()->has('loggedIn')) {
            if (session('loggedIn')) {
                return view('/cms/productrangeslideradd');
            } else {
                return Redirect('/dologout'); 
            }
        } else {
            return Redirect('/dologout'); 
        } 
    });

    // VIEW PRODUCT RANGE CARDS
    Route::get('/productrangecardview', function () {
        if (session()->has('loggedIn')) {
            if (session('loggedIn')) {
                return view('/cms/productrangecardview');
            } else {
                return Redirect('/dologout'); 
            }
        } else {
            return Redirect('/dologout'); 
        } 
    });


    // ADD PRODUCT RANGE CARD
    Route::get('/productrangecardadd', function () {
        if (session()->has('loggedIn')) {
            if (session('loggedIn')) {
                return view('/cms/productrangecardadd');
            } else {
                return Redirect('/dologout'); 
            }
        } else {
            return Redirect('/dologout'); 
        } 
    });

    // EDIT PRODUCT RANGE CARD
    Route::get('/productrangecardedit/{card}', function ($card) {
        if (session()->has('loggedIn')) {
            if (session('loggedIn')) {
                return view('/cms/productrangecardedit', ["card" => $card]);
            } else {
                return Redirect('/dologout'); 
            }
        } else {
            return Redirect('/dologout'); 
        } 
    });


/* END PRODUCT RANGE */

/* GALLERY */

    // VIEW GALLERY SLIDER IMAGES
    Route::get('/gallerysliderview', function () {
        if (session()->has('loggedIn')) {
            if (session('loggedIn')) {
                return view('/cms/gallerysliderview');
            } else {
                return Redirect('/dologout'); 
            }
        } else {
            return Redirect('/dologout'); 
        } 
    });

    // ADD GALLERY SLIDER IMAGE
    Route::get('/galleryslideradd', function () {
        if (session()->has('loggedIn')) {
            if (session('loggedIn')) {
                return view('/cms/galleryslideradd');
            } else {
                return Redirect('/dologout'); 
            }
        } else {
            return Redirect('/dologout'); 
        } 
    });

    // VIEW GALLERY IMAGES
    Route::get('/galleryimageview', function () {
        if (session()->has('loggedIn')) {
            if (session('loggedIn')) {
                return view('/cms/galleryimageview');
            } else {
                return Redirect('/dologout'); 
            }
        } else {
            return Redirect('/dologout'); 
        } 
    });

    // ADD GALLERY IMAGE
    Route::get('/galleryimageadd', function () {
        if (session()->has('loggedIn')) {
            if (session('loggedIn')) {
                return view('/cms/galleryimageadd');
            } else {
                return Redirect('/dologout'); 
            }
        } else {
            return Redirect('/dologout'); 
        } 
    });

/* END GALLERY */

/* SOCIAL */

// EDIT SOCIAL SLIDER IMAGES
Route::get('/socialheaderedit', function () {
    if (session()->has('loggedIn')) {
        if (session('loggedIn')) {
            return view('/cms/socialheaderedit');
        } else {
            return Redirect('/dologout'); 
        }
    } else {
        return Redirect('/dologout'); 
    } 
});

/* END SOCIAL */

/* CONTACT */

// EDIT CONTACT SLIDER IMAGES
Route::get('/contactheaderedit', function () {
    if (session()->has('loggedIn')) {
        if (session('loggedIn')) {
            return view('/cms/contactheaderedit');
        } else {
            return Redirect('/dologout'); 
        }
    } else {
        return Redirect('/dologout'); 
    } 
});

/* END CONTACT */

// OPERATIONS

// PERFORM LOGIN OPERATION
Route::post('/dologin', 'LoginController@login');

// PERFORM LOGOUT OPERATION
Route::get('/dologout', 'LoginController@logout');

// PERFORM REGISTER OPERATION
Route::post('/doregister', 'LoginController@register');

/* HOME */

    // PERFORM HOME SLIDER ADD OPERATION
    Route::post('/dohomeslideradd', 'UploadController@uploadHomeSliderImage');

    // PERFORM HOME SLIDER DELETE OPERATION
    Route::get('/dohomesliderdelete', 'DeleteController@deleteHomeSliderImage');

    // PERFORM HOME CARD EDIT OPERATION
    Route::post('/dohomecardedit/{card}', ['uses' => 'UploadController@editHomeCard']);

    // PERFORM HOME CARD IMAGE DELETE OPERATION
    Route::post('/dohomecardimagedelete', 'DeleteController@deleteHomeCardImage');

/* END HOME */

/* ABOUT US */

    // PERFORM ABOUT US SLIDER ADD OPERATION
    Route::post('/doaboutusslideradd', 'UploadController@uploadAboutUsSliderImage');

    // PERFORM ABOUT US SLIDER DELETE OPERATION
    Route::get('/doaboutussliderdelete', 'DeleteController@deleteAboutUsSliderImage');

    // PERFORM ABOUT US CARD ADD OPERATION
    Route::post('/doaboutuscardadd', 'UploadController@uploadAboutUsCard');

    // PERFORM ABOUT US CARD EDIT OPERATION
    Route::post('/doaboutuscardedit/{card}', ['uses' => 'UploadController@editAboutUsCard']);

    // PERFORM ABOUT US CARD DELETE OPERATION
    Route::get('/doaboutuscarddelete/{card}', ['uses' => 'DeleteController@deleteAboutUsCard']);

    // PERFORM ABOUT US CARD IMAGE DELETE OPERATION
    Route::post('/doaboutuscardimagedelete', 'DeleteController@deleteAboutUsCardImage');

/* END ABOUT US */

/* ABOUT OLIVES */

    // PERFORM ABOUT OLIVES SLIDER ADD OPERATION
    Route::post('/doaboutolivesslideradd', 'UploadController@uploadAboutOlivesSliderImage');

    // PERFORM ABOUT OLIVES SLIDER DELETE OPERATION
    Route::get('/doaboutolivessliderdelete', 'DeleteController@deleteAboutOlivesSliderImage');

    // PERFORM ABOUT OLIVES HEADER EDIT OPERATION
    Route::post('/doaboutolivesheaderedit', 'UploadController@editAboutOlivesHeader');

    // PERFORM ABOUT OLIVES CARD ADD OPERATION
    Route::post('/doaboutolivescardadd', 'UploadController@uploadAboutOlivesCard');

    // PERFORM ABOUT OLIVES CARD EDIT OPERATION
    Route::post('/doaboutolivescardedit/{card}', ['uses' => 'UploadController@editAboutOlivesCard']);

    // PERFORM ABOUT OLIVES CARD DELETE OPERATION
    Route::get('/doaboutolivescarddelete/{card}', ['uses' => 'DeleteController@deleteAboutOlivesCard']);

    // PERFORM ABOUT OLIVES CARD IMAGE DELETE OPERATION
    Route::post('/doaboutolivescardimagedelete', 'DeleteController@deleteAboutOlivesCardImage');

    // PERFORM ABOUT OLIVES FOOTER EDIT OPERATION
    Route::post('/doaboutolivesfooteredit', 'UploadController@editAboutOlivesFooter');

/* END ABOUT OLIVES */

/* RECIPES */

    // PERFORM RECIPES SLIDER ADD OPERATION
    Route::post('/dorecipesslideradd', 'UploadController@uploadRecipesSliderImage');

    // PERFORM RECIPES SLIDER DELETE OPERATION
    Route::get('/dorecipessliderdelete', 'DeleteController@deleteRecipesSliderImage');

    // PERFORM RECIPES CARD ADD OPERATION
    Route::post('/dorecipescardadd', 'UploadController@uploadRecipesCard');

    // PERFORM RECIPES CARD EDIT OPERATION
    Route::post('/dorecipescardedit/{card}', ['uses' => 'UploadController@editRecipesCard']);

    // PERFORM ABOUT OLIVES CARD DELETE OPERATION
    Route::get('/dorecipescarddelete/{card}', ['uses' => 'DeleteController@deleteRecipesCard']);

/* END RECIPES */

/* KITCHEN FUN */

    // PERFORM KITCHEN FUN HEADER EDIT OPERATION
    Route::post('/dokitchenfunheaderedit', 'UploadController@editKitchenFunHeader');

    // PERFORM RECIPES CARD ADD OPERATION
    Route::post('/dokitchenfuncardadd', 'UploadController@uploadKitchenFunCard');

    // PERFORM KITCHEN FUN CARD EDIT OPERATION
    Route::post('/dokitchenfuncardedit/{card}', ['uses' => 'UploadController@editKitchenFunCard']);

    // PERFORM KITCHEN FUN CARD DELETE OPERATION
    Route::get('/dorecipescarddelete/{card}', ['uses' => 'DeleteController@deleteRecipesCard']);

    // PERFORM KITCHEN FUN CARD IMAGE DELETE OPERATION
    Route::post('/dokitchenfuncardimagedelete', 'DeleteController@deleteKitchenFunCardImage');

/* END KITCHEN FUN */

/* PRODUCT RANGE */

    // PERFORM PRODUCT RANGE SLIDER ADD OPERATION
    Route::post('/doproductrangeslideradd', 'UploadController@uploadProductRangeSliderImage');

    // PERFORM PRODUCT RANGE SLIDER DELETE OPERATION
    Route::get('/doproductrangesliderdelete', 'DeleteController@deleteProductRangeSliderImage');

    // PERFORM PRODUCT RANGE CARD ADD OPERATION
    Route::post('/doproductrangecardadd', 'UploadController@uploadProductRangeCard');

    // PERFORM PRODUCT RANGE CARD EDIT OPERATION
    Route::post('/doproductrangecardedit/{card}', ['uses' => 'UploadController@editProductRangeCard']);

    // PERFORM PRODUCT RANGE OLIVES CARD DELETE OPERATION
    Route::get('/doproductrangecarddelete/{card}', ['uses' => 'DeleteController@deleteProductRangeCard']);

/* END PRODUCT RANGE */

/* GALLERY */

    // PERFORM GALLERY SLIDER ADD OPERATION
    Route::post('/dogalleryslideradd', 'UploadController@uploadGallerySliderImage');

    // PERFORM GALLERY SLIDER DELETE OPERATION
    Route::get('/dogallerysliderdelete', 'DeleteController@deleteGallerySliderImage');

    // PERFORM GALLERY IMAGE ADD OPERATION
    Route::post('/dogalleryimageadd', 'UploadController@uploadGalleryImage');

    // PERFORM GALLERY IMAGE DELETE OPERATION
    Route::get('/dogalleryimagedelete', 'DeleteController@deleteGalleryImage');


/* END GALLERY */

/* SOCIAL */

    // PERFORM SOCIAL HEADER EDIT OPERATION
    Route::post('/dosocialheaderedit', 'UploadController@editSocialHeaderImage');

/* END SOCIAL */

/* CONTACT */

    // PERFORM CONTACT HEADER EDIT OPERATION
    Route::post('/docontactheaderedit', 'UploadController@editContactHeaderImage');

/* END CONTACT */

