<?php

// GET ABOUT US SLIDER IMAGES
//$aboutUsSliderImages = DB::table('aboutussliderimages')->get();

// GET RECORD
$card = DB::table('userrecipescards')->where('id', $cardid)->get();

$pageTitle = $card[0]->header; 

?>
@include('layouts.header')

<body>

<h1 style="visibility: hidden; position: fixed;" class="hiddenElement">Buffet Olives - <?php echo $pageTitle; ?> - The largest Table Olive producer in South Africa</h1>

    @include('layouts.loader')

    @include('layouts.social')

    <div id="wrapper">

        <div id="recipeheaderImageParent">

            <div id="recipeHeaderWrapper">

                <img id="recipeheaderImageDetails" src="<?php echo $card[0]->imgurl; ?>" style="max-width: 100%;">

                <div id="recipeHeader" class="recipeHeaderDetails" style="padding: 30px 0 !important;">

                    <h2><?php echo $card[0]->header;?></h2>
                    <h2>Aspiring Chef: <?php echo $card[0]->description;?></h2>                   

                </div>
                <div class="clearfix"></div>

            </div>

            <div class="recipeCardDetailsParent">

                <div class="mobileOnly" style="text-align: center;">

                    <div class="card endCard recipeCardDetails recipeLeft">

                        <div class="cardTextContainer max-height: none !important;">
                            <p class="cardHeader"><strong>INGREDIENTS</strong></p>
                            <p class="cardBodyText" style="white-space: pre-line; text-align: left;"><?php echo $card[0]->ingredients; ?></p>
                        </div>
                        <div class="clearfix"></div>
                    
                    </div>

                    <div class="card endCard recipeCardDetails recipeRight">

                        <div class="cardTextContainer max-height: none !important;">
                            <p class="cardHeader"><strong>METHOD</strong></p>
                            <p class="cardBodyText" style="white-space: pre-line; text-align: left;"><?php echo $card[0]->method; ?></p>
                        </div>
                        <div class="clearfix"></div>

                    </div>

                    <div class="clearfix"></div>4

                </div>

                <div class="desktopOnly" style="text-align: center;">

                    <div class="card endCard recipeCardDetails" style="width: 80%; max-width: 100%;">

                        <div class="cardTextContainer boxshadow" style="float: left; padding: 0; width: 50%; max-height: none !important;">
                            <p class="cardHeader" style="text-align: left; display: block; padding: 40px 40px;"><strong>INGREDIENTS</strong></p>
                            <p class="cardBodyText" style="white-space: pre-line; text-align: left; padding-left: 40px; padding-bottom: 40px;"><?php echo $card[0]->ingredients; ?></p>
                        </div>

                        <div class="cardTextContainer boxshadow" style="float: right; padding: 0; width: 50%; max-height: none !important;">
                            <p class="cardHeader" style="text-align: left; display: block; padding: 40px 40px;"><strong>METHOD</strong></p>
                            <p class="cardBodyText" style="white-space: pre-line; text-align: left; padding-left: 40px; padding-bottom: 40px; padding-right: 40px;"><?php echo $card[0]->method; ?></p>
                        </div>
                        <div class="clearfix"></div>

                        

                    </div>

                    <div class="clearfix"></div>

                </div>

            </div>

        </div>  

       <div id="hamburgericonmenuwrapper">
            <div id="hamburgerui">
                <ul>
                    <li><a href="#"><span id="navtoggler"></span></a></li>
                </ul>
            </div>
            <nav id="fullscreenmenu">
                <ul>
                    <li><a href="/"><nav>Home</nav></a></li>
                    <li><a href="/aspiringchefs"><nav>Fan Recipes</nav></a></li>
                    <li><a href="/aboutus"><nav>ABOUT US</nav></a></li>
                    <li><a href="/aboutolives"><nav>ABOUT OLIVES</nav></a></li>
                    <li><a href="/recipes"><nav>RECIPES</nav></a></li>
                    <li><a href="/productrange"><nav>PRODUCT RANGE</nav></a></li>
                    <li><a href="/gallery"><nav>GALLERY</nav></a></li>
                    <li><a href="/social"><nav>SOCIAL FEED</nav></a></li>
                    <li><a href="/contact"><nav>CONTACT</nav></a></li>
                </ul>
            </nav>
        </div>

    </div>
@extends('layouts.footer')