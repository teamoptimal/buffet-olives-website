<?php $pageTitle = "Fan Recipes"; 

$boxCounter = 0;

$pageDescription = "WIN a R5 000 gift voucher from Yuppichef! Send us a picture of your own unique culinary creation, or beverage, using Buffet Olives as an ingredient to www.buffetolives.net and stand a change to WIN.";

// GET CARDS
$recipesCards = DB::table('userrecipescards')->orderBy('id', 'desc')->get();

?>

<!-- MAIN CSS FILE -->
<link rel="stylesheet" href="{{asset('css/main.css')}}">

@include('layouts.header')

<body>

<h1 style="visibility: hidden; position: fixed;" class="hiddenElement">Buffet Olives - <?php echo $pageTitle; ?> - The largest Table Olive producer in South Africa</h1>

    @include('layouts.loader')

    @include('layouts.social')

    <div id="wrapper">

        <div>

            <img id="recipeheaderImage" src="/images/aspiringchefsheader.jpg" alt="Fan Recipes">

        </div>

        <div id="recipeWrapper">

            <h1 id="galleryheader">Buffet Olives User Recipes</h1>

            <div>

                <?php foreach ($recipesCards as $card) { 
                    
                    //$recipesTotal++;   
                
                ?>
                
                <a href="<?php echo "/userrecipe/" . $card->id;?>">
                
                    <div id="recipeCard<?php //echo $recipesTotal; ?>" class="recipeCard" style="background-image: url('<?php echo $card->imgurl; ?>'); background-repeat: no-repeat; background-size: cover; background-position: center;">
            
                            <div class="recipeTextContainer">
                                <p class="recipeHeader"><strong><?php echo $card->header; ?></strong></p>
                            </div>
                            <div class="clearfix"></div>
                
                    </div>

                </a>
            
                <?php } ?>

            </div>

        </div>  

    </div>

    <div id="hamburgericonmenuwrapper">
            <div id="hamburgerui">
                <ul>
                    <li><a href="#"><span id="navtoggler"></span></a></li>
                </ul>
            </div>
            <nav id="fullscreenmenu">
                <ul>
                    <li><a href="/"><nav>Home</nav></a></li>
                    <li><a href="/aspiringchefs"><nav>Fan Recipes</nav></a></li>
                    <li><a href="/aboutus"><nav>ABOUT US</nav></a></li>
                    <li><a href="/aboutolives"><nav>ABOUT OLIVES</nav></a></li>
                    <li><a href="/recipes"><nav>RECIPES</nav></a></li>
                    <li><a href="/productrange"><nav>PRODUCT RANGE</nav></a></li>
                    <li><a href="/gallery"><nav>GALLERY</nav></a></li>
                    <li><a href="/social"><nav>SOCIAL FEED</nav></a></li>
                    <li><a href="/contact"><nav>CONTACT</nav></a></li>
                </ul>
            </nav>
        </div>

    
@extends('layouts.footer')