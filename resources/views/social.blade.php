<?php $pageTitle = "Social"; 

// GET HEADER IMAGE
$socialHeaderImage = DB::table('socialheaderimage')->where('id', 1)->get();


?>
@include('layouts.header')

<body>

<h1 style="visibility: hidden; position: fixed;" class="hiddenElement">Buffet Olives - <?php echo $pageTitle; ?> - The largest Table Olive producer in South Africa</h1>

    @include('layouts.loader')

    @include('layouts.social')

    <div>

<img id="recipeHeaderImage" src="<?php echo $socialHeaderImage[0]->imgurl; ?>" alt="<?php echo $socialHeaderImage[0]->description; ?>">

</div>

<script async src="https://walls.io/js/wallsio-widget-1.2.js" data-wallurl="https://walls.io/h5psh?nobackground=1&amp;show_header=0" data-title="My social wall" data-width="100%" data-autoheight="1" data-height="800" data-lazyload="1"></script>

    <div id="hamburgericonmenuwrapper">
            <div id="hamburgerui">
                <ul>
                    <li><a href="#"><span id="navtoggler"></span></a></li>
                </ul>
            </div>
            <nav id="fullscreenmenu">
                <ul>
                    <li><a href="/"><nav>Home</nav></a></li>
                    <li><a href="/aspiringchefs"><nav>Fan Recipes</nav></a></li>
                    <li><a href="/aboutus"><nav>ABOUT US</nav></a></li>
                    <li><a href="/aboutolives"><nav>ABOUT OLIVES</nav></a></li>
                    <li><a href="/recipes"><nav>RECIPES</nav></a></li>
                    <li><a href="/productrange"><nav>PRODUCT RANGE</nav></a></li>
                    <li><a href="/gallery"><nav>GALLERY</nav></a></li>
                    <li><a href="/social"><nav>SOCIAL FEED</nav></a></li>
                    <li><a href="/contact"><nav>CONTACT</nav></a></li>
                </ul>
            </nav>
        </div>

    
@extends('layouts.footer')