        <div id="footer">
            <div id="footer-nav-wrapper">
                <a href="/aboutus"><p class="footer-nav">ABOUT US</p></a>
                <p class="footer-nav"> | </p>
                <a href="/aboutolives"><p class="footer-nav">ABOUT OLIVES</p></a>
                <p class="footer-nav"> | </p>
                <a href="/recipes"><p class="footer-nav">RECIPES</p></a>
                <p class="footer-nav"> | </p>
                <a href="/productrange"><p class="footer-nav">PRODUCT RANGE</p></a>
                <p class="footer-nav"> | </p>
                <a href="/gallery"><p class="footer-nav">GALLERY</p></a>
                <p class="footer-nav"> | </p>
                <a href="/social"><p class="footer-nav">SOCIAL FEED</p></a>
                <p class="footer-nav"> | </p>
                <a href="/contact"><p class="footer-nav">CONTACT</p></a>
                <p class="footer-nav"> | </p>
                <a download="Buffet Olives Website Privacy Policy 27-07-21" href="{{ asset('pdfs/Website-Privacy-Policy-27-07-21.pdf') }}"><p class="footer-nav">PRIVACY POLICY</p></a>
                <p class="footer-nav"> | </p>
                <a download="BUFFET OLIVES & ROBERTSON WINERY COMPETITON T’s & C’s" href="{{ asset('pdfs/BO-and-RW-Comp-TsandCs.pdf') }}"><p class="footer-nav">T's & C's</p></a>
                
            </div>

            <div id="footer-text-wrapper">
                <p>ABOUT US</p>
                <p>Buffet Olives is the largest table olive producer in South Africa. Our olives are grown, processed and packaged on our farm, ensuring quality control each step of the way. We are passionate about olives and believe in the health benefits they offer. Our philosophy on the farm is to look after our people as well as we look after our olives, and therefore we’ve created a nurturing environment for our olives and our employees.</p> 
            </div>

            <div>

                <div id="footer-social">

                    <a href="https://www.facebook.com/buffetolives/" target="_blank"><img class="socialiconfooter" src="{{asset('images/facebook.png')}}" alt="Facebook Icon"></a>

                    <a href="https://www.instagram.com/buffetolives/" target="_blank"><img class="socialiconfooter" src="{{asset('images/instagram.png')}}" alt="Instagram Icon"></a>

                    <a href="/contact" target="_blank"><img class="socialiconfooter" src="{{asset('images/mail.png')}}" alt="Contact Us Icon"></a>

                </div>

                <div id="footer-logo">

                    <a href="/" target="_blank"><img class="footerlogoimage" src="{{asset('images/bologo.png')}}" alt="Buffet Olives Website"></a>

                </div>

            </div>

        
        </div>

        <!-- JQUERY -->
        <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>

        <script src="{{asset('js/hamburgericonmenu.js')}}">

/***********************************************
* Hamburger Icon Menu - (c) Dynamic Drive DHTML code library (www.dynamicdrive.com)
* Please keep this notice intact
* Visit Dynamic Drive at http://www.dynamicdrive.com/ for this script and 100s more
***********************************************/

</script>

        <!-- MAIN JS FILE -->
        <script src="{{asset('js/main.js')}}"></script>

        <!-- FLEXSLIDER -->
        <script src="{{asset('js/jquery.flexslider.js')}}"></script>

        <!-- MAGNIFIC -->
        <script src="{{asset('js/magnific.js')}}"></script>

        <!-- TOAST MESSAGE -->
        <script src="{{asset('js/jquery.toastmessage.js')}}"></script>

        <!-- SCROLL REVEAL -->
        <script src="https://unpkg.com/scrollreveal/dist/scrollreveal.min.js"></script>

        <script>

            window.addEventListener("load", function () {
                setTimeout(function () {
                    $('.loader-overlay').hide();
                }, 500);

            });
           
        </script>

        <script>

            // Can also be used with $(document).ready()
            $(window).on('load' ,function() {
                $('.flexslider').flexslider({
                    animation: "slide",
                    touch: false,
                    controlNav: "false",
                    directionNav: "false",
                    slideshowSpeed: "5000",
                    randomize: "true",
                    pauseOnHover: "true"
                });
            });

            $('.galleryCard').magnificPopup({
                delegate: 'div', // child items selector, by clicking on it popup will open
                type: 'image',
                gallery:{
                    enabled:true
                },
                alignTop: true
            
            });


            function closeContactPopup() {
                $('#contactComplete').hide();
            }

            $(function() {
                // Get the form.
                var form = $('#contactform');

                // Get the messages div.
                var formSubmit = $('#contactComplete');

            // Set up an event listener for the contact form.
                $(form).submit(function(event) {

                    // Stop the browser from submitting the form.
                    event.preventDefault();

                    // Serialize the form data.
                    var formData = $(form).serialize();

                    // Submit the form using AJAX.
                    $.ajax({
                        type: 'POST',
                        url: $(form).attr('action'),
                        data: formData
                    }).done(function(response) {

                        // Clear the form.
                        $('#contactForm-name').val('');
                        $('#contactForm-number').val('');
                        $('#contactForm-email').val('');
                        $('#contactForm-message').val('');

                        // POPUP CONTACT MESSAGE
                        formSubmit.show();
                    })
                }); 
            });

            <?php if (isset($recipesTotal)) { ?>

                <script>

                    var recipesTotal = <?php echo $recipesTotal; ?>;

                    if (recipesTotal > 6) {

                        for (var i = 7; i <= recipesTotal; i++) {

                            $("#recipeCard" + recipesTotal).hide();
                        
                        }

                    }

                   

                </script>

            <?php } ?>           

/*
            window.sr = new ScrollReveal();
            sr.reveal('.panel-animation-slide-left', {
                reset: true,
                distance: '70px',
                duration: 450,
                opacity: 0.5,
                scale: 1,
                origin: 'left',
                delay: 0,
                viewFactor: 0.2
            });

            sr.reveal('.panel-animation-slide-right', {
                reset: true,
                distance: '70px',
                duration: 450,
                opacity: 0.5,
                scale: 1,
                origin: 'left',
                delay: 0,
                viewFactor: 0.2
            });

              */  

        </script>

    </body>
</html>