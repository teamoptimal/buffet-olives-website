<body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="index.html" class="site_title"><span>Buffet Olives CMS</span></a>
            </div>

            <div class="clearfix"></div>

            <!-- menu profile quick info -->
            <div class="profile clearfix">
              
              <div class="profile_info">
                <span>Welcome,</span>
                <h2><?php echo session('name') . " " . session('surname'); ?></h2>
              </div>
            </div>
            <!-- /menu profile quick info -->

            <br />

            <!-- sidebar menu -->
            <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
              <div class="menu_section">
                <h3>General</h3>
                <ul class="nav side-menu">
                  <li><a><i class="fa fa-home"></i> Home <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="/homesliderview">Slider</a></li>
                      <li><a href="/homecardview">Cards</a></li>
                    </ul>
                  </li>
                  <li><a><i class="fa fa-group"></i> About Us <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="/aboutussliderview">Slider</a></li>
                      <li><a href="/aboutuscardview">Cards</a></li>
                    </ul>
                  </li>
                  <li><a><i class="fa fa-circle-o"></i> About Olives <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="/aboutolivessliderview">Slider</a></li>
                      <li><a href="/aboutolivesheaderedit">Header</a></li>
                      <li><a href="/aboutolivescardview">Cards</a></li>
                      <li><a href="/aboutolivesfooteredit">Footer</a></li>
                    </ul>
                  </li>
                  <li><a><i class="fa fa-book"></i> Recipes <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="/recipessliderview">Slider</a></li>
                      <li><a href="/recipescardview">Cards</a></li>
                    </ul>
                  </li>
                  <li><a><i class="fa fa-apple"></i> Kitchen Fun <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="/kitchenfunheaderedit">Header Image</a></li>
                      <li><a href="/kitchenfuncardview">Cards</a></li>
                    </ul>
                  </li>
                  <li><a><i class="fa fa-home"></i> Product Range <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="/productrangesliderview">Slider</a></li>
                      <li><a href="/productrangecardview">Cards</a></li>
                    </ul>
                  </li>
                  <li><a><i class="fa fa-picture-o"></i> Gallery <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="/gallerysliderview">Slider</a></li>
                      <li><a href="/galleryimageview">Images</a></li>
                    </ul>
                  </li>
                  <li><a><i class="fa fa-facebook"></i> Social <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="/socialheaderedit">Header Image</a></li>
                    </ul>
                  </li>
                  <li><a><i class="fa fa-phone"></i> Contact <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="/contactheaderedit">Header Image</a></li>
                    </ul>
                  </li>
                  
                </ul>
              </div>
              
            </div>
            <!-- /sidebar menu -->

            <!-- /menu footer buttons -->
            <div class="sidebar-footer hidden-small">
            <a data-toggle="tooltip" data-placement="top" title="Website" href="/">
                <span class="glyphicon glyphicon-globe" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="Home" href="/cmshome">
                <span class="glyphicon glyphicon-home" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="Logout" href="/dologout">
                <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
              </a>
            </div>
            <!-- /menu footer buttons -->
          </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">
          <div class="nav_menu">
            <nav>
              <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
              </div>

              <ul class="nav navbar-nav navbar-right">
                <li class="">
                  <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                    <?php echo session('name') . " " . session('surname'); ?>
                    <span class=" fa fa-angle-down"></span>
                  </a>
                  <ul class="dropdown-menu dropdown-usermenu pull-right">
                    <li><a href="/dologout"><i class="fa fa-sign-out pull-right"></i> Log Out</a></li>
                  </ul>
                </li>

              </ul>
            </nav>
          </div>
        </div>
        <!-- /top navigation -->