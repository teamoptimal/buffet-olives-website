<div id="social">
    
    <a href="https://www.facebook.com/buffetolives/" target="_blank"><img class="socialicon" src="{{asset('images/facebook.png')}}" alt="Facebook Icon"></a>

    <a href="https://www.instagram.com/buffetolives/" target="_blank"><img class="socialicon" src="{{asset('images/instagram.png')}}" alt="Instagram Icon"></a>

    <a href="/contact" target="_blank"><img class="socialicon" src="{{asset('images/mail.png')}}" alt="Contact Us Icon"></a>

</div>