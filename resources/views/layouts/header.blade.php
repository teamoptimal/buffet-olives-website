<!DOCTYPE html>
    <html>
        <head>

            <!-- Google Analytics -->
            <script>
            (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
            })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

            ga('create', 'UA-122316138-1', 'auto');
            ga('send', 'pageview');
            </script>
            <!-- End Google Analytics -->

            <!-- Global site tag (gtag.js) - Google Analytics -->
            <script async src="https://www.googletagmanager.com/gtag/js?id=UA-122316138-1"></script>
            <script>
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());

            gtag('config', 'UA-122316138-1');
            </script>

            <!-- Facebook Pixel Code -->
            <script>
            !function(f,b,e,v,n,t,s)
            {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
            n.callMethod.apply(n,arguments):n.queue.push(arguments)};
            if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
            n.queue=[];t=b.createElement(e);t.async=!0;
            t.src=v;s=b.getElementsByTagName(e)[0];
            s.parentNode.insertBefore(t,s)}(window,document,'script',
            'https://connect.facebook.net/en_US/fbevents.js');
            fbq('init', '179417954052672'); 
            fbq('track', 'PageView');
            </script>
            <noscript>
            <img height="1" width="1" 
            src="https://www.facebook.com/tr?id=179417954052672&ev=PageView
            &noscript=1"/>
            </noscript>
            <!-- End Facebook Pixel Code -->

            <meta name="facebook-domain-verification" content="bb74yecp5w1u4ml5iu7p50dvrzh24o" />

            <title>Buffet Olives - <?php echo $pageTitle; ?> - The largest Table Olive producer in South Africa</title>
            <meta name="title" content ="Buffet Olives - <?php echo $pageTitle;?> - The Largest Table Olive producer in South Africa">
            <meta name="description" content="<?php 
            if (isset($pageDescription)) {
                echo $pageDescription;
            } else {
                echo "";
            }?>">
            <meta name="keywords" content="Tables Olives, South African Olive producer, Olive Recipes, benefits of olives, health benefits of olives, calories in olives, benefits of black olives, eating olives, olives nutrition, benefits of green olives, olive tree, olive garden, olives tree, benefits of eating olives">
            <meta charset="UTF-8">
            <link rel="canonical" href="https://buffetolives.com" />
            <meta name="viewport" content="width=device-width, initial-scale=1">
            <!-- FONT AWESOME -->
            <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
            <!-- MAIN CSS FILE -->
            <link rel="stylesheet" href="{{asset('css/main.css')}}">

            <link href="http://fonts.googleapis.com/css?family=Bitter&subset=latin" rel="stylesheet" type="text/css">

            <!-- FAVICON -->
            <link rel='shortcut icon' href="{{asset('bofavicon.ico')}}" />

            <!-- ROBOTO -->
            <link href="https://fonts.googleapis.com/css?family=Roboto:400,500" rel="stylesheet">
            <link rel="stylesheet" href="{{asset('css/hamburgericonmenu.css')}}" />

            <!-- FLEX SLIDER -->
            <link rel="stylesheet" type="text/css" href="{{asset('css/flexslider.css')}}"/>

            <!-- MAGNIFIC -->
            <link rel="stylesheet" href="{{asset('css/magnific.css')}}">

            <!-- TOAST MESSAGE -->
            <link rel="stylesheet" href="{{asset('css/jquery.toastmessage.css')}}">

            <script src="https://www.google.com/recaptcha/api.js" async defer></script>

        </head>

        