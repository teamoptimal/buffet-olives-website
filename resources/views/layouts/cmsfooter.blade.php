    <!-- jQuery -->
    <script src="{{asset('/vendors/jquery/dist/jquery.min.js')}}"></script>
    <!-- Bootstrap -->
    <script src="{{asset('/vendors/bootstrap/dist/js/bootstrap.min.js')}}"></script>
    <!-- FastClick -->
    <script src="{{asset('/vendors/fastclick/lib/fastclick.js')}}"></script>
    <!-- NProgress -->
    <script src="{{asset('/vendors/nprogress/nprogress.js')}}"></script>
    <!-- iCheck -->
    <script src="{{asset('/vendors/iCheck/icheck.min.js')}}"></script>
    <!-- Datatables -->
    <script src="{{asset('/vendors/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
    <script src="{{asset('/vendors/datatables.net-buttons/js/dataTables.buttons.min.js')}}"></script>
    <script src="{{asset('/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js')}}"></script>
    <script src="{{asset('/vendors/datatables.net-buttons/js/buttons.flash.min.js')}}"></script>
    <script src="{{asset('/vendors/datatables.net-buttons/js/buttons.html5.min.js')}}"></script>
    <script src="{{asset('/vendors/datatables.net-buttons/js/buttons.print.min.js')}}"></script>
    <script src="{{asset('/vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js')}}"></script>
    <script src="{{asset('/vendors/datatables.net-keytable/js/dataTables.keyTable.min.js')}}"></script>
    <script src="{{asset('/vendors/datatables.net-responsive/js/dataTables.responsive.min.js')}}"></script>
    <script src="{{asset('/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js')}}"></script>
    <script src="{{asset('/vendors/datatables.net-scroller/js/dataTables.scroller.min.js')}}"></script>
    <script src="{{asset('/vendors/jszip/dist/jszip.min.js')}}"></script>
    <script src="{{asset('/vendors/pdfmake/build/pdfmake.min.js')}}"></script>
    <script src="{{asset('/vendors/pdfmake/build/vfs_fonts.js')}}"></script>

    <!-- Custom Theme Scripts -->
    <script src="{{asset('/js/custom.min.js')}}"></script>

    <!-- CUSTOM SCRIPTS -->

    <!-- ADD IMAGE ON CARD EDIT PAGE -->
    <script>

      var imagesCreated = 0;

      function addImageElementToCard(){

        imagesCreated++;

        // CREATE NEW FILE INPUT ELEMENT AND ADD TO PARENT
        var newImage = $("<div id='imgParent" + imagesCreated + "' class='form-group'><label class='control-label col-md-3 col-sm-3 col-xs-12' for='img' style='padding-top: 3px !important;'><i onclick='removeImageElement(" + imagesCreated + ")' class='fa fa-times fa-2x'></i><span class='required'></span></label><div class='col-md-6 col-sm-6 col-xs-12'><input type='file' id='img" + imagesCreated + "' name='img" + imagesCreated + "' required='required' class='form-control col-md-7 col-xs-12'></div></div>").appendTo("#imageParent");

        $("#imgCount").val( parseInt($("#imgCount").val(), 10) + 1);

      }

      // REMOVE SELECTED IMAGE
      function removeImageElement($image){

        $("#imgParent" + $image).remove();

        $("#imgCount").val( parseInt($("#imgCount").val(), 10) - 1);

      }

      // DELETE SELECTED CARD
      function deleteCardImage($idcard, $url) {

        $.ajax({
          method: 'POST', // Type of response and matches what we said in the route
          url: $url, // This is the url we gave in the route
          data: { 'idcard' : $idcard, _token: '{{csrf_token()}}' }, // a JSON object to send back
          success: function(response){ // What to do if we succeed
                $("#archivedImageParent" + $idcard).remove();
          },
          error: function(jqXHR, textStatus, errorThrown) { // What to do if we fail
              console.log(JSON.stringify(jqXHR));
              console.log("AJAX error: " + textStatus + ' : ' + errorThrown);
          }
      });

      }

    </script>

  </body>
</html>