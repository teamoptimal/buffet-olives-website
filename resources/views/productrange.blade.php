<?php $pageTitle = "Product Range"; 

$pageDescription = "Lemon Stuffed Olives | Anchovy Stuffed Olives | Green Olives | Green Pitted Olives | Pimiento Stuffed Olives | Queen Olives | Calamata Style Olives | Pitted Calamata Style Olives";

// GET  SLIDER IMAGES
$productRangeSliderImages = DB::table('productrangesliderimages')->get();

// GET CARDS
$productRangeCards = DB::table('productrangecards')->orderBy('id', 'asc')->get();

$boxCounter = 0;

?>
@include('layouts.header')

<body>

<h1 style="visibility: hidden; position: fixed;" class="hiddenElement">Buffet Olives - <?php echo $pageTitle; ?> - The largest Table Olive producer in South Africa</h1>

    @include('layouts.loader')

    @include('layouts.social')

    <div id="wrapper">

        <!-- Place somewhere in the <body> of your page -->
        <div class="flexslider flexsliderheader">
            <ul class="slides">
                <?php foreach ($productRangeSliderImages as $image) { ?>
                    <li>
                    <img src="<?php echo $image->imgurl; ?>" alt="<?php echo $image->description; ?>" />
                    </li>
                <?php } ?>
            </ul>
        </div>

        <?php foreach ($productRangeCards as $card) { ?>
            
            <div class="card mobileCard">

                    <img src="<?php echo $card->imgurl; ?>" class="cardImageMobile" style="height: auto; width: 100%;">
                    <div class="cardTextContainer">
                        <p class="cardHeader"><strong><?php echo $card->header; ?></strong></p>
                        <p class="cardBodyText"><?php echo $card->bodytext; ?></p>
                    </div>
                    <div class="clearfix"></div>
        
            </div>
            
        <?php } ?>

        <?php foreach ($productRangeCards as $card) { ?>
            
            <div class="card desktopCard">
                <?php

                $boxCounter++;

                if ($boxCounter % 2 == 1) { ?>
    
                    <div class="cardTextContainer" style="width: 50%;">
                        <p class="cardHeader" style="color: #547335 !important;"><strong><?php echo $card->header; ?></strong></p>
                        <p class="cardBodyText"><?php echo $card->bodytext; ?></p>
                    </div>
                    <div class="cardImageWrapper">
                        <img src="<?php echo $card->imgurl; ?>" class="floatright cardImage">
                    </div>
                    <div class="clearfix"></div>

                <?php } else { ?>
                    <div class="cardImageWrapper">
                        <img src="<?php echo $card->imgurl; ?>" class="floatleft cardImage" style="width: 50%;">
                    </div>
                    <div class="cardTextContainer"  style="width: 50%;">
                        <p class="cardHeader" style="color: #d8c967 !important;"><strong><?php echo $card->header; ?></strong></p>
                        <p class="cardBodyText"><?php echo $card->bodytext; ?></p>
                    </div>
                    <div class="clearfix"></div>

                <?php } ?>
        
            </div>
            
        <?php } ?>

        <div id="hamburgericonmenuwrapper">
            <div id="hamburgerui">
                <ul>
                    <li><a href="#"><span id="navtoggler"></span></a></li>
                </ul>
            </div>
            <nav id="fullscreenmenu">
                <ul>
                    <li><a href="/"><nav>Home</nav></a></li>
                    <li><a href="/aspiringchefs"><nav>Fan Recipes</nav></a></li>
                    <li><a href="/aboutus"><nav>ABOUT US</nav></a></li>
                    <li><a href="/aboutolives"><nav>ABOUT OLIVES</nav></a></li>
                    <li><a href="/recipes"><nav>RECIPES</nav></a></li>
                    <li><a href="/productrange"><nav>PRODUCT RANGE</nav></a></li>
                    <li><a href="/gallery"><nav>GALLERY</nav></a></li>
                    <li><a href="/social"><nav>SOCIAL FEED</nav></a></li>
                    <li><a href="/contact"><nav>CONTACT</nav></a></li>
                </ul>
            </nav>
        </div>
        
    </div>
@extends('layouts.footer')