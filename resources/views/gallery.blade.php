<?php $pageTitle = "Gallery"; 

// GET SLIDER IMAGES
$gallerySliderImages = DB::table('gallerysliderimages')->get();

// GET CARDS
$galleryCards = DB::table('galleryimages')->get();

?>
@include('layouts.header')

<body>

<h1 style="visibility: hidden; position: fixed;" class="hiddenElement">Buffet Olives - <?php echo $pageTitle; ?> - The largest Table Olive producer in South Africa</h1>

    @include('layouts.loader')

    @include('layouts.social')

    <div id="wrapper">

        <!-- Place somewhere in the <body> of your page -->
        <div class="flexslider flexsliderheader">
        <ul class="slides">
            <?php foreach ($gallerySliderImages as $image) { ?>
                <li>
                <img src="<?php echo $image->imgurl; ?>" alt="<?php echo $image->description; ?>" />
                </li>
            <?php } ?>
        </ul>
        </div>

        <div class="card galleryCard">

            <h1 id="galleryheader">Buffet Olives Gallery</h1>
              
                <?php foreach($galleryCards as $card) { ?>

                    <div href="<?php echo $card->imgurl; ?>" class="galleryImage" style="background-image: url('<?php echo $card->imgurl; ?>'); background-repeat: no-repeat; background-size: cover; background-position: center;">
                        
                    </div>

                <?php } ?>       
        </div>

        <div id="hamburgericonmenuwrapper">
            <div id="hamburgerui">
                <ul>
                    <li><a href="#"><span id="navtoggler"></span></a></li>
                </ul>
            </div>
            <nav id="fullscreenmenu">
                <ul>
                    <li><a href="/"><nav>Home</nav></a></li>
                    <li><a href="/aspiringchefs"><nav>Fan Recipes</nav></a></li>
                    <li><a href="/aboutus"><nav>ABOUT US</nav></a></li>
                    <li><a href="/aboutolives"><nav>ABOUT OLIVES</nav></a></li>
                    <li><a href="/recipes"><nav>RECIPES</nav></a></li>
                    <li><a href="/productrange"><nav>PRODUCT RANGE</nav></a></li>
                    <li><a href="/gallery"><nav>GALLERY</nav></a></li>
                    <li><a href="/social"><nav>SOCIAL FEED</nav></a></li>
                    <li><a href="/contact"><nav>CONTACT</nav></a></li>
                </ul>
            </nav>
        </div>

    </div>
@extends('layouts.footer')