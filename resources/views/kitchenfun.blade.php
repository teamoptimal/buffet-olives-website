<?php $pageTitle = "Kitchen Fun"; 

// GET HEADER IMAGE
$kitchenFunHeaderImage = DB::table('kitchenfunheaderimage')->where('id', 1)->get();

// GET CARDS
$kitchenFunCards = DB::table('kitchenfuncards')->get();

$boxCounter = 0;

?>
@include('layouts.header')

<body>

<h1 style="visibility: hidden; position: fixed;" class="hiddenElement">Buffet Olives - <?php echo $pageTitle; ?> - The largest Table Olive producer in South Africa</h1>

    @include('layouts.loader')

    @include('layouts.social')

    <div>

        <img id="recipeheaderImage" src="<?php echo $kitchenFunHeaderImage[0]->imgurl; ?>" alt="<?php echo $kitchenFunHeaderImage[0]->description; ?>">

    </div>
    <?php foreach ($kitchenFunCards as $card) { ?>
        
        <div class="card mobileCard">
            <?php

            // GET CARD IMAGES
            $kitchenFunCardsImages = DB::table('kitchenfuncardsimages')->where('idcard', $card->id)->get();

                foreach ($kitchenFunCardsImages as $image) { ?>
                    <img src="<?php echo $image->cardimgurl; ?>" class="cardImageMobile">
                <?php } ?>
                <div class="cardTextContainer">
                    <p class="cardHeader"><strong><?php echo $card->header; ?></strong></p>
                    <p class="cardBodyText"><?php echo $card->bodytext; ?></p>
                    <a target="_blank" href="<?php echo $card->pdfurl; ?>"><button class="cardButtonGreen">DOWNLOAD RECIPE</button></a>
                </div>
                <div class="clearfix"></div>
    
        </div>
        
    <?php } ?>

    <?php foreach ($kitchenFunCards as $card) { ?>
        
        <div class="card desktopCard">
            <?php

            // GET CARD IMAGES
            $kitchenFunCardsImages = DB::table('kitchenfuncardsimages')->where('idcard', $card->id)->get();

            $boxCounter++;

            if ($boxCounter % 2 == 1) { ?>

                <div class="cardTextContainer">
                    <p class="cardHeader" style="color: #547335 !important;"><strong><?php echo $card->header; ?></strong></p>
                    <p class="cardBodyText"><?php echo $card->bodytext; ?></p>
                    <a target="_blank" href="<?php echo $card->pdfurl; ?>"><button class="cardButtonGreen">DOWNLOAD RECIPE</button></a>
                </div>
                <div class="clearfix"></div>

                <?php foreach ($kitchenFunCardsImages as $image) { ?>
                <div class="cardImageWrapper" class="alignRight">
                    <img src="<?php echo $image->cardimgurl; ?>" class="cardImage floatright">
                </div>
                <?php }

            } else {

                foreach ($kitchenFunCardsImages as $image) { ?>
                <div class="cardImageWrapper" class="alignLeft">
                    <img src="<?php echo $image->cardimgurl; ?>" class="cardImage floatleft">
                </div>
                <?php } ?>
                <div class="cardTextContainer">
                    <p class="cardHeader" style="color: #547335 !important;"><strong><?php echo $card->header; ?></strong></p>
                    <p class="cardBodyText"><?php echo $card->bodytext; ?></p>
                    <a target="_blank" href="<?php echo $card->pdfurl; ?>"><button class="cardButtonYellow">DOWNLOAD RECIPE</button></a>
                </div>
                <div class="clearfix"></div> <?php

            } ?>
    
        </div>
        
    <?php } ?>

    <div id="hamburgericonmenuwrapper">
            <div id="hamburgerui">
                <ul>
                    <li><a href="#"><span id="navtoggler"></span></a></li>
                </ul>
            </div>
            <nav id="fullscreenmenu">
                <ul>
                    <li><a href="/"><nav>Home</nav></a></li>
                    <li><a href="/aspiringchefs"><nav>Fan Recipes</nav></a></li>
                    <li><a href="/aboutus"><nav>ABOUT US</nav></a></li>
                    <li><a href="/aboutolives"><nav>ABOUT OLIVES</nav></a></li>
                    <li><a href="/recipes"><nav>RECIPES</nav></a></li>
                    <li><a href="/productrange"><nav>PRODUCT RANGE</nav></a></li>
                    <li><a href="/gallery"><nav>GALLERY</nav></a></li>
                    <li><a href="/social"><nav>SOCIAL FEED</nav></a></li>
                    <li><a href="/contact"><nav>CONTACT</nav></a></li>
                </ul>
            </nav>
        </div>

    
@extends('layouts.footer')