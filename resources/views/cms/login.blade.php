
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">LOGIN</div>

                <div class="card-body">
                    <form method="POST" action="/dologin">
                        {{ csrf_field() }}

                        <br>

                        <label>Username</label> <br>
                        <input type="text" name="username" value=""> <br> <br>

                        <label>Password</label> <br>
                        <input type="password" name="password" value=""> <br> <br>

                        <input type="submit" value="LOGIN">
                        
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
