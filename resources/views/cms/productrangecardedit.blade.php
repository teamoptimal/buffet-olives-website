<?php 

$pageTitle = "Product Range Card Edit"; 

// GET CARD RECORDS
$productRangeCards = DB::table('productrangecards')->where('id', $card)->get();

?>

@extends('layouts.cmsheader')

@include('layouts.cmsinclude')

    <!-- Page Content -->
    <div class="right_col" style="min-height: 100% !important;">

        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                <h2>Product Range Card <small> Edit Card </small></h2>
                
                <div class="clearfix"></div>
                </div>
                <div class="x_content">
                <br>
                <form id="demo-form2" data-parsley-validate="" class="form-horizontal form-label-left" method="post" action="/doproductrangecardedit/<?php echo $productRangeCards[0]->id; ?>" enctype="multipart/form-data">

                    {{csrf_field()}}

                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="imgdesc"> Card Header <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="text" id="imgdesc" name="header" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo $productRangeCards[0]->header; ?>">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Card Text <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <textarea class="form-control" rows="6" name="text" required="required"><?php echo $productRangeCards[0]->bodytext; ?></textarea>
                        </div>
                    </div>

                    <div id="imageParent"></div>

                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="img" style="padding-top: 3px !important;">
                            Card Image (Leave blank to not update.)
                            <span class="required"></span> 
                        </label>
                        
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="file" id="img" name="img" class="form-control col-md-7 col-xs-12">
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-4">
                            <a href="/recipescardview" class="btn btn-primary">Cancel</a>
                            <button class="btn btn-primary" type="reset">Reset</button>
                            <button type="submit" class="btn btn-success">Submit</button>
                        </div>
                    </div>

                    <input type="hidden" id="imgCount" name="imgCount" value=0>

                </form>

                </div>
            </div>
        </div>

    </div>

@extends('layouts.cmsfooter')
