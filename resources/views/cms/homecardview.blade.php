<?php 

$pageTitle = "Home Cards"; 

// GET CARD RECORDS
$homeCards = DB::table('homecards')->get();

?>

@extends('layouts.cmsheader')

@include('layouts.cmsinclude')

    <!-- page content -->
    <div class="right_col" style="min-height: 100% !important;">
        <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3> Home Cards <small> View Cards </small> </h3>
              </div>
 
            </div>

            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12">
                <div class="x_panel">
                    <div class="x_content">
                        <div class="row">    

                            <?php 

                                foreach($homeCards as $record) { ?>

                                <?php 

                                    // GET FIRST IMAGE OF CARD
                                    $homeCardImage = DB::table('homecardsimages')->where('idcard', $record->id)->first();

                                ?>

                                    <div class="col-md-55">
                                        <div class="thumbnail">
                                            <div class="image view view-first">
                                                <img style="width: 100%; display: block;" src="<?php 
                                                if (sizeOf($homeCardImage) != 0) {
                                                    echo $homeCardImage->cardimgurl;
                                                }
                                                ?>"/>
                                                <div class="mask no-caption">
                                                    <div class="tools tools-bottom">
                                                        <a></a>
                                                        <a href="/homecardedit/<?php echo $record->id; ?>"><i class="fa fa-pencil"></i></a>
                                                        <a></a>     
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="caption">
                                                <p><strong><?php echo $record->header; ?></strong></p>
                                                <p><?php echo $record->bodytext; ?></p>
                                            </div>
                                        </div>
                                    </div>

                                <?php }

                            ?>

                        </div>
                    </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->
    </div>

@extends('layouts.cmsfooter')
