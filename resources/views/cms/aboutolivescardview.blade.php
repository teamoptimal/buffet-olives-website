<?php 

$pageTitle = "About Olives Cards"; 

// GET CARD RECORDS
$aboutOlivesCards = DB::table('aboutolivescards')->get();

?>

@extends('layouts.cmsheader')

@include('layouts.cmsinclude')

    <!-- page content -->
    <div class="right_col" style="min-height: 100% !important;">
        <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3> About Olives Cards <small> View Cards </small> </h3>
              </div>
 
            </div>

            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12">
                <div class="x_panel">

                    <!-- ADD BUTTONS HERE -->
                    <a href="/aboutolivescardadd" class="btn btn-success" style="margin-top: 20px !important;">Add Card</a>
                    <div class="x_content">

                        <div class="row">    

                            <?php 

                                foreach($aboutOlivesCards as $record) { ?>

                                <?php 

                                    // GET FIRST IMAGE OF CARD
                                    $aboutOlivesCardImage = DB::table('aboutolivescardsimages')->where('idcard', $record->id)->first();

                                ?>

                                    <div class="col-md-55">
                                        <div class="thumbnail">
                                            <div class="image view view-first">
                                                <img style="width: 100%; display: block;" src="<?php 
                                                if (sizeOf($aboutOlivesCardImage) != 0) {
                                                    echo $aboutOlivesCardImage->cardimgurl;
                                                }
                                                ?>"/>
                                                <div class="mask no-caption">
                                                    <div class="tools tools-bottom">
                                                        <a></a>
                                                        <a href="/aboutolivescardedit/<?php echo $record->id; ?>"><i class="fa fa-pencil"></i></a>
                                                        <a href="/doaboutolivescarddelete/<?php echo $record->id; ?>"><i class="fa fa-times"></i></a>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="caption">
                                                <p><strong><?php echo $record->header; ?></strong></p>
                                                <p><?php echo $record->bodytext; ?></p>
                                            </div>
                                        </div>
                                    </div>

                                <?php }

                            ?>

                        </div>
                    </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->
    </div>

@extends('layouts.cmsfooter')
