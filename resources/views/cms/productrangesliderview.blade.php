<?php 

$pageTitle = "Product Range Slider Images"; 

// GET PRODUCT RANGE SLIDER IMAGES
$productRangeSliderImages = DB::table('productrangesliderimages')->get();

?>

@extends('layouts.cmsheader')

@include('layouts.cmsinclude')

    <!-- page content -->
    <div class="right_col" style="min-height: 100% !important;">
        <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3> Product Range Slider <small> View Images </small> </h3>
              </div>
 
            </div>

            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12">
                <div class="x_panel">

                    <!-- ADD BUTTONS HERE -->
                    <a href="/productrangeslideradd" class="btn btn-success" style="margin-top: 20px !important;">Add Image</a>

                    <div class="ln_solid"></div>

                    <div class="x_content">
                     
                        <div class="row"> 

                            <?php 

                                foreach($productRangeSliderImages as $record) { ?>

                                    <div class="col-md-55">
                                        <div class="thumbnail">
                                            <div class="image view view-first">
                                                <img style="width: 100%; display: block;" src="<?php echo $record->imgurl; ?>" alt=<?php echo $record->description; ?> />
                                                <div class="mask no-caption">
                                                    <div class="tools tools-bottom">
                                                        <a></a>
                                                        <a href="/doproductrangesliderdelete?img=<?php echo $record->id; ?>"><i class="fa fa-times"></i></a>
                                                        <a></a>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="caption">
                                                <p><?php echo $record->description; ?></p>
                                            </div>
                                        </div>
                                    </div>

                                <?php }

                            ?>

                        </div>
                    </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->
    </div>

@extends('layouts.cmsfooter')
