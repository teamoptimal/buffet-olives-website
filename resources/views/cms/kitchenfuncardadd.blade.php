<?php 

$pageTitle = "Kitchen Fun Card Add"; 

?>

@extends('layouts.cmsheader')

@include('layouts.cmsinclude')

    <!-- Page Content -->
    <div class="right_col" style="min-height: 100% !important;">

        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                <h2>Kitchen Fun Card <small> Add Card </small></h2>
                
                <div class="clearfix"></div>
                </div>
                <div class="x_content">
                <br>
                <form id="demo-form2" data-parsley-validate="" class="form-horizontal form-label-left" method="post" action="/dokitchenfuncardadd" enctype="multipart/form-data">

                    {{csrf_field()}}

                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="imgdesc"> Card Header <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="text" id="imgdesc" name="header" required="required" class="form-control col-md-7 col-xs-12" value="">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Card Text <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <textarea class="form-control" rows="6" name="text" required="required"></textarea>
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="img" style="padding-top: 3px !important;">
                            Recipe PDF
                            <span class="required"></span> 
                        </label>
                        
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="file" id="pdf" name="pdf" required="required" class="form-control col-md-7 col-xs-12">
                        </div>
                    </div>

                    <div id="imageParent"></div>

                    <div class="form-group">
                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-5" style="margin-top: 20px; margin-bottom: 20px;">
                        <button type="button" class="btn btn-success btn-lg" onclick="addImageElementToCard();">Add Image</button>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-4">
                            <a href="/aboutolivescardview" class="btn btn-primary">Cancel</a>
                            <button class="btn btn-primary" type="reset">Reset</button>
                            <button type="submit" class="btn btn-success">Submit</button>
                        </div>
                    </div>

                    <input type="hidden" id="imgCount" name="imgCount" value=0>

                </form>

                    <div class="col-md-12">
                        <div class="x_panel">
                            <div class="x_content">
                                <div class="row">

                                
                                   
                                </div>
                            </div>
                        </div>
                    </div>   

                </div>
            </div>
        </div>

    </div>

@extends('layouts.cmsfooter')
