<?php $pageTitle = "Contact"; 

$pageDescription = "Buffet Olives is the largest table olive producer in South Africa. Our olive trees and olives are grown, processed and packaged on our farm, ensuring quality control each step of the way.";

// GET CONTACT HEADER IMAGE
$contactHeaderImage = DB::table('contactheaderimage')->where('id', 1)->get();

?>
@include('layouts.header')

<body>

<h1 style="visibility: hidden; position: fixed;" class="hiddenElement">Buffet Olives - <?php echo $pageTitle; ?> - The largest Table Olive producer in South Africa</h1>

    @include('layouts.loader')

    @include('layouts.social')

    <div id="wrapper">

        <img id="contactHeaderImage" src="<?php echo $contactHeaderImage[0]->imgurl; ?>" alt="<?php echo $contactHeaderImage[0]->description; ?>">

        <h1 id="contactHeader">Contact Us</h1>

        <div id="contactWrapper">

        <div id="contactComplete">

            <i class="fa fa-times fa-3x" style="color: #ffffff;" onclick="closeContactPopup()"></i>

            <img src="{{asset('/images/contactolive.png')}}">

            <p>Thank you for your message!</p>

        </div>

            <div id="contactDetails">

                    <p class="contactText">CONTACT : Jennifer Marais </p>
                    <p class="contactText">TELEPHONE: +27 (0)21 8683120</p>
                    <p class="contactText"><a href="mailto:jennifer@buffetolives.net" style="color: inherit; text-decoration: none;">EMAIL: jennifer@buffetolives.net</a></p>

                    <p class="contactText">FOLLOW US: <a href="https://www.facebook.com/buffetolives/" target="_blank"><i style="color: #547335;" class="fa fa-facebook"></i></a>  |  <a href="https://www.instagram.com/buffetolives/" target="_blank"><i style="color: #547335;" class="fa fa-instagram"></i></a></p>

            </div>

            <div id="contactForm">

                <form method="post" action="/send" id="contactform">

                    {{csrf_field()}}

                    <input type="text" name="name" id="contactForm-name" placeholder="YOUR NAME" required></input>

                    <br>

                    <input type="text" name="number" id="contactForm-number" placeholder="CONTACT NUMBER" required></input>

                    <br>

                    <input type="email" name="email" id="contactForm-email" placeholder="EMAIL ADDRESS" required></input>

                    <br>

                    <textarea rows="4" name="usermessage" id="contactForm-message" placeholder="YOUR MESSAGE" required></textarea>

                    <br>

                    <div class="g-recaptcha"
                        style="text-align:center;display: inline-block;"
                        data-sitekey="6Lf2TAgbAAAAAJhIZeM-jyXlvo4nMCsyssEXhMa9">
                    </div>
                    
                    <br/>
                        <button type="submit" value="SUBMIT" class="btnContact" id="submitBtn">SEND</button>
                    

                </form>

            </div>

        </div>

       <div id="hamburgericonmenuwrapper">
            <div id="hamburgerui">
                <ul>
                    <li><a href="#"><span id="navtoggler"></span></a></li>
                </ul>
            </div>
            <nav id="fullscreenmenu">
                <ul>
                    <li><a href="/"><nav>Home</nav></a></li>
                    <li><a href="/aspiringchefs"><nav>Fan Recipes</nav></a></li>
                    <li><a href="/aboutus"><nav>ABOUT US</nav></a></li>
                    <li><a href="/aboutolives"><nav>ABOUT OLIVES</nav></a></li>
                    <li><a href="/recipes"><nav>RECIPES</nav></a></li>
                    <li><a href="/productrange"><nav>PRODUCT RANGE</nav></a></li>
                    <li><a href="/gallery"><nav>GALLERY</nav></a></li>
                    <li><a href="/social"><nav>SOCIAL FEED</nav></a></li>
                    <li><a href="/contact"><nav>CONTACT</nav></a></li>
                </ul>
            </nav>
        </div>

    </div>

    <script>
        document.getElementById("contactform").addEventListener("submit",function(evt){
    
            var response = grecaptcha.getResponse();

            if(response.length == 0){ 
                //reCaptcha not verified
                alert('please verify you are human by checking the "I\'m not a robot" box'); 
                evt.preventDefault();
                return false;
            } else {
                document.getElementById("submitBtn").innerHTML = 'MESSAGE SENT'
            }
        
        });
    </script>
@extends('layouts.footer')