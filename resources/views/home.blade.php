<?php $pageTitle = "Home"; 

$pageDescription = "Buffet Olives is the largest table olive producer in South Africa. Our olive trees and olives are grown, processed and packaged on our farm, ensuring quality control each step of the way.";

// GET HOME SLIDER IMAGES
$homeSliderImages = DB::table('homesliderimages')->get();

// GET HOME CARDS
$homeCards = DB::table('homecards')->orderBy('id', 'asc')->get();

$boxCounter = 0;

?>
@include('layouts.header')

<body>

<h1 style="visibility: hidden; position: fixed;" class="hiddenElement">Buffet Olives - <?php echo $pageTitle; ?> - The largest Table Olive producer in South Africa</h1>

    @include('layouts.loader')

    @extends('layouts.social')

    <div id="wrapper">

        <!-- Place somewhere in the <body> of your page -->
        <div class="flexslider flexsliderheader">
            <ul class="slides">
                <!-- <?php //foreach ($homeSliderImages as $image) { ?>
                    <li class="flexsliderheaderimageparent">
                        <img class="flexsliderheaderimage" src="<?php //echo $image->imgurl; ?>" alt="<?php //echo $image->description; ?>" />
                    </li>
                <?php //} ?> -->
                <li class="flexsliderheaderimageparent">
                    <img class="flexsliderheaderimage" src="{{asset('images/not-just-an-olive.jpg')}}" alt="Buffet Olives" />
                </li>
            </ul>
        </div>

        <?php foreach ($homeCards as $card) { ?>
            
            <div class="card mobileCard panel-animation-slide-up">
                <?php

                // GET CARD IMAGES
                $homeCardsImages = DB::table('homecardsimages')->where('idcard', $card->id)->get(); ?>

                <div class="flexslider">
                    <ul class="slides">
                        <?php foreach ($homeCardsImages as $image) { ?>
                            <li>
                                <img src="<?php echo $image->cardimgurl; ?>" class="cardImageMobile">
                            </li>
                        <?php } ?>
                    </ul>
                </div>

                
                <div class="cardTextContainer">
                    <p class="cardHeader"><strong><?php echo $card->header; ?></strong></p>
                    <p class="cardBodyText"><?php echo $card->bodytext; ?></p>
                    <a href="<?php echo $card->navurl; ?>"><button class="cardButtonGreen">FIND OUT MORE</button></a>
                </div>
                <div class="clearfix"></div>
        
            </div>
            
        <?php } ?>

        <?php foreach ($homeCards as $card) { ?>
            
            <div class="card desktopCard panel-animation-slide-up">
                <?php

                // GET CARD IMAGES
                $homeCardsImages = DB::table('homecardsimages')->where('idcard', $card->id)->get();

                $boxCounter++;

                if ($boxCounter % 2 == 1) { ?>

                    <div class="cardTextContainer">
                        <p class="cardHeader" style="color: #547335 !important;"><strong><?php echo $card->header; ?></strong></p>
                        <p class="cardBodyText"><?php echo $card->bodytext; ?></p>
                            <a href="<?php echo $card->navurl; ?>"><button class="cardButtonGreen">FIND OUT MORE</button></a>
                    </div>
                    <div class="clearfix"></div>

                    <div class="cardImageWrapper" class="alignRight">
                        <div class="flexslider">
                            <ul class="slides">
                                <?php foreach ($homeCardsImages as $image) { ?>
                                    <li>
                                    
                                        <img src="<?php echo $image->cardimgurl; ?>" class="cardImage">
                                    
                                    </li>
                                <?php } ?>
                            </ul>
                        </div>
                    </div>

                <?php } else { ?>

                    <div class="cardImageWrapper" class="alignRight">
                        <div class="flexslider">
                            <ul class="slides">
                                <?php foreach ($homeCardsImages as $image) { ?>
                                    <li>
                                    
                                        <img src="<?php echo $image->cardimgurl; ?>" class="cardImage">
                                    
                                    </li>
                                <?php } ?>
                            </ul>
                        </div>
                    </div>
                    <div class="cardTextContainer">
                        <p class="cardHeader" style="color: #d8c967 !important;"><strong><?php echo $card->header; ?></strong></p>
                        <p class="cardBodyText"><?php echo $card->bodytext; ?></p>
  
                            <a href="<?php echo $card->navurl; ?>"><button class="cardButtonYellow">FIND OUT MORE</button></a>
                    </div>
                    <div class="clearfix"></div> <?php

                } ?>
        
            </div>
            
        <?php } ?>

        <div id="hamburgericonmenuwrapper">
            <div id="hamburgerui">
                <ul>
                    <li><a href="#"><span id="navtoggler"></span></a></li>
                </ul>
            </div>
            <nav id="fullscreenmenu">
                <ul>
                    <li><a href="/"><nav>Home</nav></a></li>
                    <li><a href="/aspiringchefs"><nav>Fan Recipes</nav></a></li>
                    <li><a href="/aboutus"><nav>ABOUT US</nav></a></li>
                    <li><a href="/aboutolives"><nav>ABOUT OLIVES</nav></a></li>
                    <li><a href="/recipes"><nav>RECIPES</nav></a></li>
                    <li><a href="/productrange"><nav>PRODUCT RANGE</nav></a></li>
                    <li><a href="/gallery"><nav>GALLERY</nav></a></li>
                    <li><a href="/social"><nav>SOCIAL FEED</nav></a></li>
                    <li><a href="/contact"><nav>CONTACT</nav></a></li>
                </ul>
            </nav>
        </div>

    </div>
@extends('layouts.footer')