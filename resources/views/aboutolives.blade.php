<?php $pageTitle = "About Olives"; 

$pageDescription = "We produce five main cultivars on our farm namely: Mission, Manzanilla, Barouni, Kalamata and Nocellara Del Belice. Each olive as delicious and fragrant as the next. Buffet Olives provides olives suited to every taste, occasion and mood. The health benefits of Olives are amazing";

// GET ABOUT OLIVES SLIDER IMAGES
$aboutOlivesSliderImages = DB::table('aboutolivessliderimages')->get();

// GET ABOUT OLIVES CARDS
$aboutOlivesCards = DB::table('aboutolivescards')->orderBy('id', 'asc')->get();

$boxCounter = 0;

?>
@include('layouts.header')

<body>

<h1 style="visibility: hidden; position: fixed;" class="hiddenElement">Buffet Olives - <?php echo $pageTitle; ?> - The largest Table Olive producer in South Africa</h1>

    @include('layouts.loader')

    @include('layouts.social')

    <!-- Place somewhere in the <body> of your page -->
    <div class="flexslider flexsliderheader">
        <ul class="slides">
            <?php foreach ($aboutOlivesSliderImages as $image) { ?>
                <li class="flexsliderheaderimageparent">
                    <img class="flexsliderheaderimage" src="<?php echo $image->imgurl; ?>" alt="<?php echo $image->description; ?>" />
                </li>
            <?php } ?>
        </ul>
    </div>

    <div class="card endCard">
        <?php

        // GET CARD IMAGES
        $aboutOlivesHeader = DB::table('aboutolivesheader')->where('id', 1)->get(); ?>

        <div class="cardTextContainer" style="width: 90%;">
            <p class="cardHeader"><strong><?php echo $aboutOlivesHeader[0]->heading; ?></strong></p>
            <p class="cardBodyText"><?php echo $aboutOlivesHeader[0]->text; ?></p>
        </div>
        <div class="clearfix"></div>
    
    </div>

    <?php foreach ($aboutOlivesCards as $card) { ?>
        
        <div class="card mobileCard">
            <?php

            // GET CARD IMAGES
            $aboutOlivesCardsImages = DB::table('aboutolivescardsimages')->where('idcard', $card->id)->get(); ?>

            <div class="flexslider">
                <ul class="slides">
                    <?php foreach ($aboutOlivesCardsImages as $image) { ?>
                        <li>
                            <img src="<?php echo $image->cardimgurl; ?>" class="cardImageMobile">
                        </li>
                    <?php } ?>
                </ul>
            </div>
                <div class="cardTextContainer">
                    <p class="cardHeader" style="color: #547335 !important;"><strong><?php echo $card->header; ?></strong></p>
                    <p class="cardBodyText"><?php echo $card->bodytext; ?></p>
                </div>
                <div class="clearfix"></div>
    
        </div>
        
    <?php } ?>

    <?php foreach ($aboutOlivesCards as $card) { ?>
        
        <div class="card desktopCard">
            <?php

            // GET CARD IMAGES
            $aboutOlivesCardsImages = DB::table('aboutolivescardsimages')->where('idcard', $card->id)->get();

            $boxCounter++;

            if ($boxCounter % 2 == 1) { ?>

                <div class="cardTextContainer">
                    <p class="cardHeader" style="color: #547335 !important;"><strong><?php echo $card->header; ?></strong></p>
                    <p class="cardBodyText"><?php echo $card->bodytext; ?></p>
                </div>
                <div class="clearfix"></div>

                <div class="cardImageWrapper" class="alignRight">
                    <div class="flexslider">
                        <ul class="slides">
                            <?php foreach ($aboutOlivesCardsImages as $image) { ?>
                                <li>
                                
                                    <img src="<?php echo $image->cardimgurl; ?>" class="cardImage">
                                
                                </li>
                            <?php } ?>
                        </ul>
                    </div>
                </div>

            <?php } else { ?>

                <div class="cardImageWrapper" class="alignRight">
                    <div class="flexslider">
                        <ul class="slides">
                            <?php foreach ($aboutOlivesCardsImages as $image) { ?>
                                <li>
                                
                                    <img src="<?php echo $image->cardimgurl; ?>" class="cardImage">
                                
                                </li>
                            <?php } ?>
                        </ul>
                    </div>
                </div>
                <div class="cardTextContainer">
                    <p class="cardHeader" style="color: #d8c967 !important;"><strong><?php echo $card->header; ?></strong></p>
                    <p class="cardBodyText"><?php echo $card->bodytext; ?></p>
                </div>
                <div class="clearfix"></div> <?php

            } ?>
    
        </div>
        
    <?php } ?>

    <div class="card endCard">
        <?php

        // GET CARD IMAGES
        $aboutOlivesFooter = DB::table('aboutolivesfooter')->where('id', 1)->get(); ?>

        <div class="cardTextContainer" style="width: 90%;">
            <p class="cardHeader" style="color: #547335 !important;"><strong><?php echo $aboutOlivesFooter[0]->heading; ?></strong></p>
            <p class="cardBodyText"><?php echo $aboutOlivesFooter[0]->text; ?></p>
        </div>
        <div class="clearfix"></div>

         <div id="hamburgericonmenuwrapper">
            <div id="hamburgerui">
                <ul>
                    <li><a href="#"><span id="navtoggler"></span></a></li>
                </ul>
            </div>
            <nav id="fullscreenmenu">
                <ul>
                    <li><a href="/"><nav>Home</nav></a></li>
                    <li><a href="/aspiringchefs"><nav>Fan Recipes</nav></a></li>
                    <li><a href="/aboutus"><nav>ABOUT US</nav></a></li>
                    <li><a href="/aboutolives"><nav>ABOUT OLIVES</nav></a></li>
                    <li><a href="/recipes"><nav>RECIPES</nav></a></li>
                    <li><a href="/productrange"><nav>PRODUCT RANGE</nav></a></li>
                    <li><a href="/gallery"><nav>GALLERY</nav></a></li>
                    <li><a href="/social"><nav>SOCIAL FEED</nav></a></li>
                    <li><a href="/contact"><nav>CONTACT</nav></a></li>
                </ul>
            </nav>
        </div>
    
    </div>

@extends('layouts.footer')